/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.events;

import javafx.beans.NamedArg;
import javafx.event.EventType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;

public class MouseEventFactory {

    private EventType<? extends MouseEvent> m_eventType;
    private double m_x;
    private double m_y;
    private double m_screenX;
    private double m_screenY;
    private MouseButton m_button;
    private int m_clickCount;
    private boolean m_shiftDown;
    private boolean m_controlDown;
    private boolean m_altDown;
    private boolean m_metaDown;
    private boolean m_primaryButtonDown;
    private boolean m_middleButtonDown;
    private boolean m_secondaryButtonDown;
    private boolean m_synthesized;
    private boolean m_popupTrigger;
    private boolean m_stillSincePress;
    private PickResult m_pickResult;

    public static MouseEventFactory copy(MouseEvent p_mouseEvent) {
        return new MouseEventFactory(p_mouseEvent);
    }

    /**
     * Constructs new MouseEvent event with null source and target.
     * @param eventType The type of the event.
     * @param x The x with respect to the scene.
     * @param y The y with respect to the scene.
     * @param screenX The x coordinate relative to screen.
     * @param screenY The y coordinate relative to screen.
     * @param button the mouse button used
     * @param clickCount number of click counts
     * @param shiftDown true if shift modifier was pressed.
     * @param controlDown true if control modifier was pressed.
     * @param altDown true if alt modifier was pressed.
     * @param metaDown true if meta modifier was pressed.
     * @param primaryButtonDown true if primary button was pressed.
     * @param middleButtonDown true if middle button was pressed.
     * @param secondaryButtonDown true if secondary button was pressed.
     * @param synthesized if this event was synthesized
     * @param popupTrigger whether this event denotes a popup trigger for current platform
     * @param stillSincePress see {@link #isStillSincePress() }
     * @param pickResult pick result. Can be null, in this case a 2D pick result
     *                   without any further values is constructed
     *                   based on the scene coordinates
     * @since JavaFX 8.0
     */
    public MouseEventFactory(
        @NamedArg("eventType") EventType<? extends MouseEvent> eventType,
        @NamedArg("x") double x, @NamedArg("y") double y,
        @NamedArg("screenX") double screenX, @NamedArg("screenY") double screenY,
        @NamedArg("button") MouseButton button,
        @NamedArg("clickCount") int clickCount,
        @NamedArg("shiftDown") boolean shiftDown,
        @NamedArg("controlDown") boolean controlDown,
        @NamedArg("altDown") boolean altDown,
        @NamedArg("metaDown") boolean metaDown,
        @NamedArg("primaryButtonDown") boolean primaryButtonDown,
        @NamedArg("middleButtonDown") boolean middleButtonDown,
        @NamedArg("secondaryButtonDown") boolean secondaryButtonDown,
        @NamedArg("synthesized") boolean synthesized,
        @NamedArg("popupTrigger") boolean popupTrigger,
        @NamedArg("stillSincePress") boolean stillSincePress,
        @NamedArg("pickResult") PickResult pickResult) {

        this.m_eventType = eventType;
        this.m_x = x;
        this.m_y = y;
        this.m_screenX = screenX;
        this.m_screenY = screenY;
        this.m_button = button;
        this.m_clickCount = clickCount;
        this.m_shiftDown = shiftDown;
        this.m_controlDown = controlDown;
        this.m_altDown = altDown;
        this.m_metaDown = metaDown;
        this.m_primaryButtonDown = primaryButtonDown;
        this.m_middleButtonDown = middleButtonDown;
        this.m_secondaryButtonDown = secondaryButtonDown;
        this.m_synthesized = synthesized;
        this.m_popupTrigger = popupTrigger;
        this.m_stillSincePress = stillSincePress;
        this.m_pickResult = pickResult;
    }


    public MouseEventFactory(MouseEvent p_original) {
        this.m_eventType = p_original.getEventType();
        this.m_x = p_original.getX();
        this.m_y = p_original.getY();
        this.m_screenX = p_original.getScreenX();
        this.m_screenY = p_original.getScreenY();
        this.m_button = p_original.getButton();
        this.m_clickCount = p_original.getClickCount();
        this.m_shiftDown = p_original.isShiftDown();
        this.m_controlDown = p_original.isControlDown();
        this.m_altDown = p_original.isAltDown();
        this.m_metaDown = p_original.isMetaDown();
        this.m_primaryButtonDown = p_original.isPrimaryButtonDown();
        this.m_middleButtonDown = p_original.isMiddleButtonDown();
        this.m_secondaryButtonDown = p_original.isSecondaryButtonDown();
        this.m_synthesized = p_original.isSynthesized();
        this.m_popupTrigger = p_original.isPopupTrigger();
        this.m_stillSincePress = p_original.isStillSincePress();
        this.m_pickResult = p_original.getPickResult();
    }
    public MouseEvent generate() {
        return new MouseEvent(this.m_eventType, this.m_x, this.m_y, this.m_screenX, this.m_screenY,
                              this.m_button, this.m_clickCount, this.m_shiftDown,
                              this.m_controlDown,

                              this.m_altDown,

                              this.m_metaDown, this.m_primaryButtonDown,
                              this.m_middleButtonDown,

                              this.m_secondaryButtonDown, this.m_synthesized,
                              this.m_popupTrigger,

                              this.m_stillSincePress, this.m_pickResult);
    }

    public void setEventType(final EventType<? extends MouseEvent>
                                 p_eventType) {
        this.m_eventType = p_eventType;
    }

    public void setX(final double p_x) {
        this.m_x = p_x;
    }

    public void setY(final double p_y) {
        this.m_y = p_y;
    }

    public void setScreenX(final double p_screenX) {
        this.m_screenX = p_screenX;
    }

    public void setScreenY(final double p_screenY) {
        this.m_screenY = p_screenY;
    }

    public void setButton(final MouseButton p_button) {
        this.m_button = p_button;
    }

    public void setClickCount(final int p_clickCount) {
        this.m_clickCount = p_clickCount;
    }

    public void setShiftDown(final boolean p_shiftDown) {
        this.m_shiftDown = p_shiftDown;
    }

    public void setControlDown(final boolean p_controlDown) {
        this.m_controlDown = p_controlDown;
    }

    public void setAltDown(final boolean p_altDown) {
        this.m_altDown = p_altDown;
    }

    public void setMetaDown(final boolean p_metaDown) {
        this.m_metaDown = p_metaDown;
    }

    public void setPrimaryButtonDown(final boolean p_primaryButtonDown) {
        this.m_primaryButtonDown = p_primaryButtonDown;
    }

    public void setMiddleButtonDown(final boolean p_middleButtonDown) {
        this.m_middleButtonDown = p_middleButtonDown;
    }

    public void setSecondaryButtonDown(final boolean p_secondaryButtonDown) {
        this.m_secondaryButtonDown = p_secondaryButtonDown;
    }

    public void setSynthesized(final boolean p_synthesized) {
        this.m_synthesized = p_synthesized;
    }

    public void setPopupTrigger(final boolean p_popupTrigger) {
        this.m_popupTrigger = p_popupTrigger;
    }

    public void setStillSincePress(final boolean p_stillSincePress) {
        this.m_stillSincePress = p_stillSincePress;
    }

    public void setPickResult(final PickResult p_pickResult) {
        this.m_pickResult = p_pickResult;
    }
}
