/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import de.rtiersch.logging.Log;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;


public class TraverseMonitor implements EventHandler<KeyEvent> {

    private final ArrayList<Node> m_traverseOrder;
    private final Log m_log = Log.getInstance();
    private int m_lastTraversedElement;

    public TraverseMonitor() {
        this.m_traverseOrder = new ArrayList<>();
    }
    public TraverseMonitor(ArrayList<Node> p_traversable) {
        this.m_traverseOrder = p_traversable;
    }

    @Override
    public void handle(final KeyEvent p_event) {
        if (p_event.getCode() == KeyCode.TAB) {
            this.m_log.debug("Tab action detected.", "TraverseMonitor");
            p_event.consume();
            final Object l_dest = p_event.getTarget();
            if (this.m_traverseOrder.contains(l_dest)) {
                this.m_log.debug("Tab pressed on an traversable Element",
                    "TraverseMonitor");
                final int l_oldIndex = this.m_traverseOrder.indexOf(l_dest);
                if (p_event.isShiftDown()) {
                    if (l_oldIndex > 0) {
                        this.m_log.debug("Decrease traverse-index.",
                            "TraverseMonitor");
                        this.m_lastTraversedElement = l_oldIndex - 1;
                    } else {
                        this.m_log.debug("Resetting traverse index to last "
                                + "element.",
                            "TraverseMonitor");
                        this.m_lastTraversedElement = this.m_traverseOrder.size()
                            - 1;
                    }
                } else {
                    if (l_oldIndex < (this.m_traverseOrder.size() - 1)) {
                        this.m_log.debug("Increasing traverse-index.",
                            "TraverseMonitor");
                        this.m_lastTraversedElement = l_oldIndex + 1;
                    } else {
                        this.m_log.debug("Resetting traverse index to first "
                                + "element",
                            "TraverseMonitor");
                        this.m_lastTraversedElement = 0;
                    }
                }
            } else {
                if (p_event.isShiftDown()) {
                    if (this.m_lastTraversedElement == 0) {
                        this.m_lastTraversedElement = this.m_traverseOrder
                            .size() - 1;
                    } else {
                        this.m_lastTraversedElement--;
                    }
                } else {
                    if (this.m_lastTraversedElement == (this.m_traverseOrder
                        .size() - 1)) {
                        this.m_lastTraversedElement = 0;
                    } else {
                        this.m_lastTraversedElement++;
                    }
                }
            }
            this.m_traverseOrder.get(this.m_lastTraversedElement).requestFocus();
        }
    }
    public void registerAtEnd(@NotNull final Node... p_nodes) {
        final int l_size = p_nodes.length;
        this.m_traverseOrder.ensureCapacity(l_size);
        this.m_traverseOrder.addAll(Arrays.asList(p_nodes));
    }
}
