/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic;

import javafx.scene.layout.AnchorPane;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Base class of CTSSStringContent. Substitution to partial classes.
 */
public abstract class ATSSCBase
    extends AnchorPane
    implements IScroll {


    /**
     * Afterscroll Events.
     */
    @NotNull
    private final ArrayList<IScroll> m_afterScrollEvents;

    /**
     * The base of CTSStringContent.
     */
    public ATSSCBase() {
        this.m_afterScrollEvents = new ArrayList<>(1);
    }

    @Override
    public void shiftTable(
        final int p_pixelHorizontal, final int p_pixelVertical) {
        this.pShiftTable(p_pixelHorizontal, p_pixelVertical);
        for (final IScroll l_afterScrollEvent : this.m_afterScrollEvents) {
            // Shift CTSStringHeader stuff
            l_afterScrollEvent.shiftTable(p_pixelHorizontal, p_pixelVertical);
        }

        // At first move the table.
        // At second check if there is need to move a row or column

        // Case: Tableborder reached. Scrolling not possible.
        // Case: Last cell is already loaded. Preloading not possible.
        // Case: Scrolling normally possible.
    }

    /**
     * Adds an event to be executed after this table has finished its scroll
     * thingies.
     * @param p_doOnScroll Method that will be invoked.
     */
    public void registerAfterScrollEvent(@NotNull final IScroll p_doOnScroll) {
        this.m_afterScrollEvents.add(p_doOnScroll);
    }

    /**
     * Moves the visual table relative to left / right and top / bottom. Not
     * optimized for big values, because it does not reinitialize the
     * pre-loaded values before it moves. This is the intern mapper for
     * normal scroll behaviour and not for jumps. To jump to a different
     * position in the table please use repositionTable().
     *
     * @param p_pixelHorizontal Left (-) / right (+) shift.
     * @param p_pixelVertical   Top (-) / bottom (+) shift.
     */
    protected abstract void pShiftTable(
        int p_pixelHorizontal, int p_pixelVertical);
}
