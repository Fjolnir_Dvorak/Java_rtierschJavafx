/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.stream.Collectors;

/**
 * Creates an editable ComboBox which will have a filtered dropdown list with
 * auto-completion features.
 *
 * @param <T>
 */
public class FilteredComboBox<T>
        extends ComboBox<T>
        implements EventHandler<KeyEvent> {

    /**
     * List with elements of {@link super..
     */
    private final ObservableList<T> m_data;
    /**
     * Does the caret need to be moved?
     * TODO can be deleted and treated local.
     */
    private boolean m_moveCaretToPos;
    /**
     * Position of the caret.
     */
    private int m_caretPos;

    /**
     * Creates a default FilteredComboBox instance with the provided items
     * list and
     * a default {@link #selectionModelProperty() selection model}.
     * @param p_items Initial items.
     */
    public FilteredComboBox(final ObservableList<T> p_items) {
        super(p_items);
        this.m_data = super.getItems();

        super.setEditable(true);
        super.setOnKeyReleased(FilteredComboBox.this);
    }

    /**
     * Creates a new filtered ComboBox.
     */
    public FilteredComboBox() {
        this.m_data = super.getItems();

        super.setEditable(true);
        super.setOnKeyReleased(FilteredComboBox.this);
    }

    @Override
    public final void handle(final KeyEvent p_event) {

        switch (p_event.getCode()) {
            case UP:
                this.m_caretPos = -1;
                this.moveCaret(super.getEditor().getText().length());
                return;
            case DOWN:
                if (!super.isShowing()) {
                    super.show();
                }
                this.m_caretPos = -1;
                this.moveCaret(super.getEditor().getText().length());
                return;
            case BACK_SPACE:
                this.m_moveCaretToPos = true;
                this.m_caretPos = super.getEditor().getCaretPosition();
                break;
            case DELETE:
                this.m_moveCaretToPos = true;
                this.m_caretPos = super.getEditor().getCaretPosition();
                break;
        }

        if ((p_event.getCode() == KeyCode.RIGHT) ||
            (p_event.getCode() == KeyCode.LEFT) || p_event.isControlDown() ||
            (p_event.getCode() == KeyCode.HOME) ||
            (p_event.getCode() == KeyCode.END) ||
            (p_event.getCode() == KeyCode.TAB)) {
            return;
        }

        final ObservableList<T> l_list = FXCollections.observableArrayList();
        l_list.addAll(this.m_data.stream()
                .filter(aData -> aData.toString()
                        .toLowerCase()
                        .startsWith(FilteredComboBox.super.getEditor()
                                .getText()
                                .toLowerCase()))
                .collect(Collectors.toList()));
        final String l_text = super.getEditor().getText();

        super.setItems(l_list);
        super.getEditor().setText(l_text);
        if (!this.m_moveCaretToPos) {
            this.m_caretPos = -1;
        }
        super.hide();
        this.moveCaret(l_text.length());
        if (!l_list.isEmpty()) {
            super.show();
        }
        p_event.consume();
    }

    /**
     * Moves the caret to {@code this.m_caretPos}.
     * @param p_textLength Maximal possible position of the caret.
     */
    private void moveCaret(final int p_textLength) {
        if (this.m_caretPos == -1) {
            super.getEditor().positionCaret(p_textLength);
        } else {
            super.getEditor().positionCaret(this.m_caretPos);
        }
        this.m_moveCaretToPos = false;
    }

}
