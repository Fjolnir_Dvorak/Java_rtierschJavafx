/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import de.rtiersch.datatypes.notnull.container.Size;
import de.rtiersch.logging.Log;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

public final class FxUtil {

    private static Size s_maxMonitorSize;

    private FxUtil() {

    }

    public static Size getMaxMonitorSize() {
        if (FxUtil.s_maxMonitorSize != null) {
            return new Size(FxUtil.s_maxMonitorSize);
        }
        final GraphicsEnvironment l_ge = GraphicsEnvironment
            .getLocalGraphicsEnvironment();
        final GraphicsDevice[]    l_gd = l_ge.getScreenDevices();
        final StringBuilder l_sb = new StringBuilder();
        int l_width = 0;
        int l_height = 0;
        for (int l_i = 0; l_i < l_gd.length; l_i++) {
            final DisplayMode l_dm = l_gd[l_i].getDisplayMode();
            l_sb.append(l_i)
                .append(", width: ")
                .append(l_dm.getWidth())
                .append(", height: ")
                .append(l_dm.getHeight())
                .append("\n");
            if (l_dm.getWidth() > l_width) {
                l_width = l_dm.getWidth();
            }
            if (l_dm.getHeight() > l_height) {
                l_height = l_dm.getHeight();
            }
        }
        Log.inst().info(l_sb.toString(), "DISPLAYS");
        FxUtil.s_maxMonitorSize = new Size(l_width, l_height);
        return new Size(FxUtil.s_maxMonitorSize);
    }
}
