/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic.init;

import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components.CCellHeader;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components.CCellLoaded;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components.CCellOptimized;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.CTSStringContent;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import org.jetbrains.annotations.NotNull;

/**
 * Header layer and container for CTSS (Class) Table Simple String.
 */
public class CTSStringHeader extends AnchorPane {
    /**
     * Content wrapped by the header pane.
     */
    @NotNull
    public final CTSStringContent m_tableContent;
    /**
     * Horizontal header.
     * Gets their width out of CTSStringContent.
     */
    private final CCellHeader[] m_guiHeaderH;
    /**
     * Vertical header.
     */
    private final CCellHeader[] m_guiHeaderV;
    /**
     * Width for vertical header.
     */
    public int m_guiHeaderVWidth;
    /**
     * Empty box to display between horizontal and vertical header.
     */
    public final Node m_emptyBoxTL;
    /**
     * Is the horizontal header visible or active?
     */
    public final SimpleBooleanProperty m_headerVisibleH;
    /**
     * Is the vertical header visible or active?
     */
    public final SimpleBooleanProperty m_headerVisibleV;
    /**
     * Start position for CTSSContent on the X axis.
     */
    public final SimpleDoubleProperty m_widthPosition;
    /**
     * Start position for CTSSContent on the Y axis.
     */
    public final SimpleDoubleProperty m_heightPosition;
    /**
     * Clip to cut away unseen content / overflow.
     */
    @NotNull
    private final Rectangle m_clip;


    /**
     * Intern data to display.
     */
    @NotNull
    public final InformationList<String> m_data;

    public CTSStringHeader(@NotNull final CTSStringContent p_content,
                           final boolean p_headerHVisible,
                           final boolean p_headerVVisible) {
        this.m_tableContent = p_content;
        this.m_headerVisibleH = new SimpleBooleanProperty(p_headerHVisible);
        this.m_headerVisibleV = new SimpleBooleanProperty(p_headerVVisible);
        this.m_data = this.m_tableContent.getDataTable();
        this.m_heightPosition = new SimpleDoubleProperty(0);
        this.m_widthPosition = new SimpleDoubleProperty(0);
        this.m_clip = new Rectangle(0, 0);
        super.setClip(this.m_clip);
        super.setManaged(false);
        final int l_lengthPreloadedH = this.m_tableContent.getGuiCells()
                                                          .amountColumns();
        final int l_lengthPreloadedV = this.m_tableContent.getGuiCells()
                                                          .amountRows();

        // calculate width of VHeader.
        this.m_guiHeaderVWidth = CTSSInit.InitCalculateHeaderColumnWidth(
            this.m_data.getRows(),
            50,
            this.m_tableContent.m_poolCells.m_cellMaxWidth);

        // TODO check. Could those two be actually the same?
        this.m_widthPosition.set(this.m_guiHeaderVWidth);
        // TODO think about that: What to do with headers that are to long?
        // TODO I need to calculate the height with eventual text wrapping.
        this.m_heightPosition.set(CTSSInit.GUI_CELL_DEFAULT_HEIGHT);

        // Preload horizontal header
        this.m_guiHeaderH = new CCellHeader[l_lengthPreloadedH];
        for (int l_i = 0; l_i < l_lengthPreloadedH; l_i++) {
            final CCellLoaded l_firstCell = this.m_tableContent.getGuiCells()
                .get(0, l_i);
            final int l_loadedCellIndex = l_firstCell.getLoadedCellColumn();
            final int l_width = (int) l_firstCell.getPrefWidth();
            final int l_height = CTSSInit.GUI_CELL_DEFAULT_HEIGHT;
            final String l_text = this.m_data.getColumn(l_loadedCellIndex).getHeader();
            final Color l_backgroundColour = Color.LAVENDER;
            final Font l_font = CTSSInit.GUI_FONT;
            final CCellHeader l_cell = new CCellHeader(l_loadedCellIndex,
                l_width, l_height, l_text, l_backgroundColour, l_font);
            this.m_guiHeaderH[l_i] = l_cell;

            final double l_positionLeft = AnchorPane.getLeftAnchor
                (l_firstCell) + this.m_guiHeaderVWidth;
            AnchorPane.setTopAnchor(l_cell, 0.0);
            AnchorPane.setLeftAnchor(l_cell, l_positionLeft);
        }

        // Preload vertical header
        this.m_guiHeaderV = new CCellHeader[l_lengthPreloadedV];
        for (int l_i = 0; l_i < l_lengthPreloadedV; l_i++) {
            final CCellLoaded l_firstCell = this.m_tableContent.getGuiCells()
                .get(l_i, 0);
            final int l_loadedCellIndex = l_firstCell.getLoadedCellRow();
            final int l_width = this.m_guiHeaderVWidth;
            final int l_height = (int) l_firstCell.getHeight();
            final String l_text = this.m_data.getRow(l_loadedCellIndex)
                .getHeader();
            final Color l_backgroundColour = Color.LAVENDER;
            final Font l_font = CTSSInit.GUI_FONT;
            final CCellHeader l_cell = new CCellHeader(l_loadedCellIndex,
                l_width, l_height, l_text, l_backgroundColour, l_font);
            this.m_guiHeaderV[l_i] = l_cell;

            final double l_positionTop = AnchorPane.getTopAnchor(l_firstCell)
                + CTSSInit.GUI_CELL_DEFAULT_HEIGHT;
            AnchorPane.setTopAnchor(l_cell, l_positionTop);
            AnchorPane.setLeftAnchor(l_cell, 0.0);
        }

        // create empty top corner.
        final CCellOptimized l_empty = new CCellOptimized();
        l_empty.setEditable(false);
        l_empty.setPrefHeight(CTSSInit.GUI_CELL_DEFAULT_HEIGHT);
        l_empty.setPrefWidth(this.m_guiHeaderVWidth);
        AnchorPane.setLeftAnchor(l_empty, 0.0);
        AnchorPane.setTopAnchor(l_empty, 0.0);
        this.m_emptyBoxTL = l_empty;

        // Add all children to this pane.
        super.getChildren().addAll(this.m_guiHeaderH);
        super.getChildren().addAll(this.m_guiHeaderV);
        super.getChildren().add(this.m_tableContent);
        super.getChildren().add(this.m_emptyBoxTL);

        // Initialize scroll behaviour.
        this.m_headerVisibleH.addListener(this::doOnHeaderVisibleH);
        this.m_headerVisibleV.addListener(this::doOnHeaderVisibleV);
        this.m_tableContent.prefHeightProperty().bind(super.prefHeightProperty()
            .subtract(this.m_heightPosition));
        this.m_tableContent.prefWidthProperty().bind(super.prefWidthProperty()
            .subtract(this.m_widthPosition));

        // Initialize size change
        super.prefWidthProperty().addListener(this::doOnParentWidthChanged);
        super.prefHeightProperty().addListener(this::doOnParentHeightChanged);

        // Initialize position of child.
        this.doOnHeaderVisibleH(null, !this.m_headerVisibleH.get(),
            this.m_headerVisibleH.get());
        this.doOnHeaderVisibleV(null, !this.m_headerVisibleV.get(),
            this.m_headerVisibleV.get());
        this.m_tableContent.registerAfterScrollEvent(this::doOnScroll);
    }

    private void doOnScroll(
        final int p_pixelHorizontal, final int p_pixelVertical) {
        if (p_pixelHorizontal != 0){
            // TODO shift horizontal thing
            for (int l_i = 0; l_i < this.m_guiHeaderH.length; l_i++) {
                // dynamic width, fix height.
                final int l_map = this.m_tableContent.getGuiCells()
                    .getMappingColumn(l_i);
                final CCellHeader l_cCellHeader = this.m_guiHeaderH[l_map];
                final @NotNull CCellLoaded l_cell =
                    this.m_tableContent.getGuiCells().get(0, l_i);
                // Start positioning at offset m_guiHeaderVWidth.
                final double l_position = AnchorPane
                    .getLeftAnchor(l_cell) + this.m_guiHeaderVWidth;
                final int l_loaded = l_cell.getLoadedCellColumn();
                final int l_width = (int) l_cell.getWidth();

                l_cCellHeader.setLoadedCell(l_loaded, l_width,
                    CTSSInit.GUI_CELL_DEFAULT_HEIGHT,
                    this.m_data.getColumn(l_loaded).getHeader(),
                    Color.LAVENDER);
                AnchorPane.setLeftAnchor(l_cCellHeader, l_position);
            }
        }
        if (p_pixelVertical != 0) {
            // TODO shift vertical thing
            for (int l_i = 0; l_i < this.m_guiHeaderV.length; l_i++) {
                // fix width and height
                final int l_map = this.m_tableContent.getGuiCells()
                    .getMappingRow(l_i);
                final CCellHeader l_cCellHeader = this.m_guiHeaderV[l_map];
                final @NotNull CCellLoaded l_cell =
                    this.m_tableContent.getGuiCells().get(l_i, 0);
                // Start positioning at offset m_guiHeaderHeight.
                final double l_position = AnchorPane
                    .getTopAnchor(l_cell) + CTSSInit.GUI_CELL_DEFAULT_HEIGHT;
                final int l_loaded = l_cell.getLoadedCellRow();
                final int l_width = this.m_guiHeaderVWidth;

                l_cCellHeader.setLoadedCell(l_loaded, l_width,
                    CTSSInit.GUI_CELL_DEFAULT_HEIGHT,
                    this.m_data.getRow(l_loaded).getHeader(),
                    Color.LAVENDER);
                AnchorPane.setTopAnchor(l_cCellHeader, l_position);
            }
        }
    }

    private void doOnHeaderVisibleV(
        final ObservableValue<? extends Boolean> p_observable,
        final Boolean p_oldValue,
        final Boolean p_newValue) {
        if (p_oldValue == p_newValue) {
            return;
        }
        if (p_newValue) {
            this.m_widthPosition.set(this.m_guiHeaderVWidth);
        } else {
            this.m_widthPosition.set(0);
        }
        AnchorPane.setLeftAnchor(this.m_tableContent,
            this.m_widthPosition.get());

    }
    private void doOnHeaderVisibleH(
        final ObservableValue<? extends Boolean> p_observable,
        final Boolean p_oldValue,
        final Boolean p_newValue) {
        if (p_oldValue == p_newValue) {
            return;
        }
        if (p_newValue) {
            this.m_heightPosition.set(CTSSInit.GUI_CELL_DEFAULT_HEIGHT);
        } else {
            this.m_heightPosition.set(0);
        }
        AnchorPane.setTopAnchor(this.m_tableContent,
            this.m_heightPosition.get());
    }
    /**
     * The size of CTSString hass changed.
     *
     * @param p_observable Observed Value. Not needed.
     * @param p_oldValue   old Value.
     * @param p_newValue   new Value.
     */
    public void doOnParentWidthChanged(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue) {
        this.m_clip.setWidth((double) p_newValue);
        // TODO check borders
    }

    /**
     * The size of CTSString has changed.
     *
     * @param p_observable Observed Value. Not needed.
     * @param p_oldValue   old Value.
     * @param p_newValue   new Value.
     */
    public void doOnParentHeightChanged(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue) {
        //super.setHeight((Double) p_newValue);
        this.m_clip.setHeight((double) p_newValue);
        // TODO check borders
    }


    public final CCellHeader getCellHeaderH(final int p_index) {
        return this.m_guiHeaderH[this.m_tableContent.getGuiCells()
            .getMappingColumn(p_index)];
    }
    public final CCellHeader getCellHeaderV(final int p_index) {
        return this.m_guiHeaderV[this.m_tableContent.getGuiCells()
            .getMappingRow(p_index)];
    }


    public double getHeaderWidth() {
        return this.m_guiHeaderVWidth;
    }
}
