/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic;

import de.rtiersch.datatypes.enums.EOrientation;
import de.rtiersch.datatypes.notnull.lists.array.VirtualArrayRowColumn;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.EStatus;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IColumn;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IRow;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell;
import de.rtiersch.logging.Log;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components
    .CCellLoaded;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.init.CTSSInit;
import de.rtiersch.util.DoSomething;
import de.rtiersch.util.enums.ELeftRight;
import de.rtiersch.util.enums.EUpDown;
import de.rtiersch.util.tuple.TablePosition;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.apache.commons.lang3.NotImplementedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Visualization class for datatype InformationList.
 * <p>
 * Checklist:
 * TODO modificable Cells (extendable)
 * TODO Add Column
 * TODO Remove Column
 * TODO Multi state table
 */
public class CTSStringContent
    extends ATSSCBase
    implements IColumnPositioner
{
    final Log.CountingLock m_lock = new Log.CountingLock("TableShift");


    /**
     * Cell Pool container.
     */
    @NotNull
    public final CPoolCells m_poolCells;
    /**
     * Data Pool container.
     */
    @NotNull
    public final CPoolData  m_poolData;

    /**
     * The maximal width that could be displayed in pixel.
     */
    private final int                   m_guiDisplayMaxWidth;
    /**
     * The maximal height that could be displayed in pixel.
     */
    private final int                   m_guiDisplayMaxHeight;
    /**
     * Width of the shown table in the UI in pixel.
     */
    @NotNull
    private final SimpleIntegerProperty m_guiTableWidth;
    /**
     * Height of the shown table in the UI in pixel.
     */
    @NotNull
    private final SimpleIntegerProperty m_guiTableHeight;

    /**
     * Clip to cut away unseen content / overflow.
     */
    @NotNull
    private final Rectangle m_clip;


    /**
     * Creates a new GUI Table based on JavaFX.
     *
     * @param p_maxWidth  Maximal width of this table.
     * @param p_maxHeight Maximal height of this table.
     * @param p_dataTable Model to observe and to display.
     */
    public CTSStringContent(
        final int p_maxWidth,
        final int p_maxHeight,
        @NotNull final InformationList<String> p_dataTable)
    {
        this(p_maxWidth, p_maxHeight, 0, p_dataTable);
    }

    /**
     * Creates a new GUI Table based on JavaFX.
     *
     * @param p_maxWidth     Maximal width of this table.
     * @param p_maxHeight    Maximal height of this table.
     * @param p_cellMinWidth Minimal width of the columns.
     * @param p_data         Model to observe and to display.
     */
    public CTSStringContent(
        final int p_maxWidth,
        final int p_maxHeight,
        final int p_cellMinWidth,
        @NotNull final InformationList<String> p_data)
    {
        this(p_maxWidth, p_maxHeight, p_cellMinWidth, 400, p_data);
    }

    /**
     * Creates a new GUI Table based on JavaFX.
     *
     * @param p_maxWidth     Maximal width of this table.
     * @param p_maxHeight    Maximal height of this table.
     * @param p_cellMinWidth Minimal width of the columns.
     * @param p_data         Model to observe and to display.
     */
    public CTSStringContent(
        final int p_maxWidth,
        final int p_maxHeight,
        final int p_cellMinWidth,
        final int p_cellMaxWidth,
        @NotNull final InformationList<String> p_data)
    {
        final int l_maxAmountRows = p_data.sizeRows();
        this.m_guiDisplayMaxWidth = p_maxWidth;
        this.m_guiDisplayMaxHeight = p_maxHeight;

        final ArrayList<DoubleProperty> l_widthColumns =
            CTSSInit.InitCalculateColumnWidthProperty(p_data, p_cellMinWidth,
                                                      p_cellMaxWidth);
        final ArrayList<ObservableDoubleValue> l_positionColumns =
            CTSSInit.CalculatePositions(l_widthColumns);

        final ArrayList<Integer> l_heightRows =
            CTSSInit.InitCalculateColumnHeight(p_data);
        final ArrayList<Integer> l_heightRowsSum =
            CTSSInit.CalculateSum(l_heightRows);
        final SimpleDoubleProperty l_heightMax =
            new SimpleDoubleProperty(l_heightRowsSum.get(l_maxAmountRows - 1));

        final int l_toGenLR =
            CTSSInit.CalculatePreloadLR(this.m_guiDisplayMaxWidth,
                                        p_cellMinWidth, p_data.lengthRow());
        final int l_toGenTD =
            CTSSInit.CalculatePreloadTD(this.m_guiDisplayMaxHeight,
                                        p_data.lengthColumn());
        final CCellLoaded[][] l_cells =
            CTSSInit.InitPreloadCells(l_toGenTD, l_toGenLR, p_data,
                                      l_heightRows, l_heightRowsSum,
                                      l_widthColumns, l_positionColumns);
        this.m_poolCells = new CPoolCells(
            new VirtualArrayRowColumn<>(l_cells, EOrientation.RowColumn),
            p_cellMinWidth, p_cellMaxWidth);

        p_data.addListenerCellChanged(this::doOnIListenerCellChanged);
        p_data.addListenerAddRemoveColumn(this::doOnListenerAddRemoveColumn);
        p_data.addListenerAddRemoveRow(this::doOnListenerAddRemoveRow);
        p_data.addListenerCellChangedColour(
            this::doOnIListenerCellChangedColour);

        this.addCellsToPane(l_cells);
//        super.widthProperty().addListener(this::doOnParentWidthChanged);
//        super.heightProperty().addListener(this::doOnParentHeightChanged);
        this.m_clip = new Rectangle(0, 0);
        super.setClip(this.m_clip);
        super.setManaged(true);

        super.prefWidthProperty().addListener(this::doOnParentWidthChanged);
        super.prefHeightProperty().addListener(this::doOnParentHeightChanged);

        this.m_guiTableWidth = new SimpleIntegerProperty();
        this.m_guiTableHeight = new SimpleIntegerProperty();
        final SimpleDoubleProperty l_widthMax = new SimpleDoubleProperty(
            l_positionColumns.get(l_positionColumns.size() - 1).get());
        this.m_poolData = new CPoolData(p_data, l_widthColumns, l_heightRows,
                                        l_positionColumns, l_heightRowsSum,
                                        l_heightMax, l_widthMax);
    }

    /**
     * Adds all the cells to this Pane.
     * Please only call this once at startup. It is not intended for reuse.
     *
     * @param p_cells Cells of loaded table.
     */
    private void addCellsToPane(final CCellLoaded[][] p_cells)
    {
        for (final CCellLoaded[] l_cells : p_cells) {
            super.getChildren().addAll(l_cells);
        }
    }

    /**
     * The size of CTSString hass changed.
     *
     * @param p_observable Observed Value. Not needed.
     * @param p_oldValue   old Value.
     * @param p_newValue   new Value.
     */
    public void doOnParentWidthChanged(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue)
    {
        this.m_clip.setWidth((double) p_newValue);
        this.m_guiTableWidth.set(p_newValue.intValue());
        // TODO check borders
    }

    /**
     * The size of CTSString hass changed.
     *
     * @param p_observable Observed Value. Not needed.
     * @param p_oldValue   old Value.
     * @param p_newValue   new Value.
     */
    public void doOnParentHeightChanged(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue)
    {
        this.m_clip.setHeight((double) p_newValue);
        this.m_guiTableHeight.set(p_newValue.intValue());
        // TODO check borders
    }

    /**
     * Scrolls the cells and moves the overflows to the opposite sites.
     * Warning: Input has to be validated before. No border checkings inside
     * this method.
     *
     * @param p_pixelHorizontal Left (-) / right (+) shift.
     * @param p_pixelVertical   Top (-) / bottom (+) shift.
     */
    @Override
    protected void pShiftTable(
        final int p_pixelHorizontal, final int p_pixelVertical)
    {
        // Overscrolling is caught by the scrollbars.
        this.internShiftTable(p_pixelHorizontal, p_pixelVertical);

        this.m_lock.add();

        while (this.pDoCheckAfterShift(p_pixelHorizontal, p_pixelVertical)) {
            DoSomething.doNothing(); // Could do that all day
            // Do nothing because it is already done. VOODOO-magic oO
        }

        this.m_lock.remove();

        // At first move the table.
        // At second check if there is need to move a row or column

        // Case: Tableborder reached. Scrolling not possible.
        // Case: Last cell is already loaded. Preloading not possible.
        // Case: Scrolling normally possible.
    }

    /**
     * Shifting all gui cells with the given amount in pixel.
     * Efficiency: n²
     *
     * @param p_pixelHorizontal Pixel to move left / right.
     * @param p_pixelVertical   Pixel to move top / down.
     */
    protected void internShiftTable(
        final int p_pixelHorizontal, final int p_pixelVertical)
    {
        this.m_poolData.m_positionXStart += p_pixelHorizontal;
        this.m_poolData.m_positionYStart += p_pixelVertical;

        for (int l_column = 0;
             l_column < this.m_poolCells.m_guiCells.amountColumns();
             l_column++) {
            for (int l_row = 0;
                 l_row < this.m_poolCells.m_guiCells.amountRows();
                 l_row++) {
                this.m_poolCells.m_guiCells.get(l_row, l_column)
                                           .addToPosition(-p_pixelHorizontal,
                                                          -p_pixelVertical);
            }
        }
    }

    /**
     * Checks for overflow of rows / columns and moves them to the apropiate
     * position.
     *
     * @param p_pixelHorizontal Movement in the horizontal direction.
     * @param p_pixelVertical   Movement in the vertical direction.
     * @return true if action was performed. need to recheck again...
     */
    private boolean pDoCheckAfterShift(
        final int p_pixelHorizontal, final int p_pixelVertical)
    {

        final double l_overflowLeft;
        final double l_overflowTop;
        final double l_overflowRight;
        final double l_overflowBottom;
        boolean l_lastReachedRight = false;
        boolean l_lastReachedLeft = false;
        boolean l_lastReachedTop = false;
        boolean l_lastReachedBottom = false;
        @Nullable ELeftRight l_moveLR = null;
        @Nullable EUpDown l_moveTD = null;
        // Initialize Variables.
        final CCellLoaded l_leftTopCell = this.m_poolCells.m_guiCells.get(0, 0);
        final CCellLoaded l_rightBottomCell = this.m_poolCells.m_guiCells.get(
            this.m_poolCells.m_guiCells.amountRows() - 1,
            this.m_poolCells.m_guiCells.amountColumns() - 1);
        l_overflowLeft = -1 * AnchorPane.getLeftAnchor(l_leftTopCell);
        l_overflowTop = -1 * AnchorPane.getTopAnchor(l_leftTopCell);
        l_overflowRight = AnchorPane.getLeftAnchor(l_rightBottomCell)
            + l_rightBottomCell.getWidth() - this.m_guiDisplayMaxWidth;
        l_overflowBottom = AnchorPane.getTopAnchor(l_rightBottomCell)
            + l_rightBottomCell.getHeight() - this.m_guiDisplayMaxHeight;
        if (l_leftTopCell.getLoadedCellColumn() == 0) {
            l_lastReachedLeft = true;
        }
        if (l_leftTopCell.getLoadedCellRow() == 0) {
            l_lastReachedTop = true;
        }
        if (l_rightBottomCell.getLoadedCellRow()
            >= this.m_poolData.m_dataTable.lengthColumn() - 1)
        {
            l_lastReachedBottom = true;
        }
        if (l_rightBottomCell.getLoadedCellColumn()
            == this.m_poolData.m_dataTable.lengthRow() - 1)
        {
            l_lastReachedRight = true;
        }
        if (p_pixelHorizontal > 0) {
            l_moveLR = ELeftRight.RIGHT;
        } else if (p_pixelHorizontal < 0) {
            l_moveLR = ELeftRight.LEFT;
        }
        if (p_pixelVertical > 0) {
            l_moveTD = EUpDown.DOWN;
        } else if (p_pixelVertical < 0) {
            l_moveTD = EUpDown.UP;
        }

        // Left / Right scrolling event. Moves Columns which are on the
        // passive side to the active side to increase the preload buffer
        // capacity.
        final int l_maxBufferPassiveSideLR =
            (int) (CTSSInit.PRELOAD_BALANCE * this.m_guiDisplayMaxWidth
                - this.m_guiDisplayMaxWidth) / 2;
        if (ELeftRight.RIGHT == l_moveLR) {
            // Scrolling to the right. Bigger preloading on the right hand side.
            if (l_overflowLeft > l_maxBufferPassiveSideLR) {
                // Left side is bigger than preload
                if (!l_lastReachedRight) {
                    // Border not yet reached.
                    this.pMoveCellsLeftRight(ELeftRight.RIGHT);
                    Log.Debug("Moving cells to the right hand side", "SHIFT");
                    return true;
                }
                Log.Debug("Last element reached on the right hand " + "side",
                          "SHIFT");
            }
        } else if (ELeftRight.LEFT == l_moveLR) {
            // Scrolling to the left. Bigger preloading on the left hand side.
            if (l_overflowRight > l_maxBufferPassiveSideLR) {
                // Right side is bigger than preload
                if (!l_lastReachedLeft) {
                    // Border not yet reached.
                    this.pMoveCellsLeftRight(ELeftRight.LEFT);
                    Log.Debug("Moving cells to the left hand side", "SHIFT");
                    return true;
                }
                Log.Debug("Last element reached on the left hand side",
                          "SHIFT");
            }
        }

        // THe same as above for Top-Down scroll movement.
        final int l_maxBufferPassiveSideTD =
            (int) (CTSSInit.PRELOAD_BALANCE * this.m_guiDisplayMaxHeight
                - this.m_guiDisplayMaxHeight) / 2;
        if (EUpDown.DOWN == l_moveTD) {
            // Scrolling to the right. Bigger preloading on the right hand side.
            if (l_overflowTop > l_maxBufferPassiveSideTD) {
                // Left side is bigger than preload
                if (!l_lastReachedBottom) {
                    // Border not yet reached.
                    this.pMoveCellsUpDown(EUpDown.DOWN);
                    return true;
                }
            }
        } else if (EUpDown.UP == l_moveTD) {
            // Scrolling to the left. Bigger preloading on the left hand side.
            if (l_overflowBottom > l_maxBufferPassiveSideTD) {
                // Right side is bigger than preload
                if (!l_lastReachedTop) {
                    // Border not yet reached.
                    this.pMoveCellsUpDown(EUpDown.UP);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Moves a column from left to right or vice versa.
     *
     * @param p_leftRight In which direction should it be moved?
     */
    protected void pMoveCellsLeftRight(final ELeftRight p_leftRight)
    {
        final int l_last = this.m_poolCells.m_guiCells.amountColumns() - 1;
        switch (p_leftRight) {
            case LEFT:
                final int l_firstUsedColumn =
                    this.m_poolCells.m_guiCells.get(0, 0).getLoadedCellColumn();
                Log.Debug("First loaded: " + l_firstUsedColumn, "LEFT");
                this.m_poolCells.m_guiCells.moveColumn(l_last, 0);
                this.pUpdateColumnContent(0, l_firstUsedColumn - 1);
                break;
            case RIGHT:
                final int l_lastUsedColumn =
                    this.m_poolCells.m_guiCells.get(0, l_last)
                                               .getLoadedCellColumn();
                Log.Debug("Last loaded: " + l_lastUsedColumn, "RIGHT");
                this.m_poolCells.m_guiCells.moveColumn(0, l_last);
                this.pUpdateColumnContent(l_last, l_lastUsedColumn + 1);
                break;
        }
        // Move the cells in the gui table
        // call update column
    }

    /**
     * Moves a row from the bottom to the top or vice versa.
     *
     * @param p_upDown Direction to where to move the cells.
     */
    protected void pMoveCellsUpDown(final EUpDown p_upDown)
    {
        final int l_last = this.m_poolCells.m_guiCells.amountRows() - 1;
        switch (p_upDown) {
            case UP:
                final int l_firstUsedRow =
                    this.m_poolCells.m_guiCells.get(0, 0).getLoadedCellRow();
                this.m_poolCells.m_guiCells.moveRow(l_last, 0);
                this.pUpdateRowContent(0, l_firstUsedRow - 1);
                break;
            case DOWN:
                final int l_lastUsedRow =
                    this.m_poolCells.m_guiCells.get(l_last, 0)
                                               .getLoadedCellRow();
                this.m_poolCells.m_guiCells.moveRow(0, l_last);
                this.pUpdateRowContent(l_last, l_lastUsedRow + 1);
                break;
        }
    }

    /**
     * Rewrite the visual rules for the textfield of p_guiColumn with content
     * p_newColumn (Text, width, height, background-colour, text-colour).
     *
     * @param p_guiColumn Graphic column to reinitialize.
     * @param p_newColumn Cell-contents to feed into the column.
     */
    protected void pUpdateColumnContent(
        final int p_guiColumn, final int p_newColumn)
    {
        // Load new cell contents into the column.
        final double l_width =
            this.m_poolData.m_widthColumns.get(p_newColumn).get();
        // Amount row = Cells in column
        for (int l_i = 0; l_i < this.m_poolCells.m_guiCells.amountRows(); l_i++)
        {
            final @Nullable CCellLoaded l_guiCell =
                this.m_poolCells.m_guiCells.get(l_i, p_guiColumn);
            final int l_row = l_guiCell.getLoadedCellRow();
            final ICell<String> l_cell =
                this.m_poolData.m_dataTable.getColumnRow(p_newColumn, l_row);
            final int l_height = this.m_poolData.m_heightRows.get(l_row);
            l_guiCell.setLoadedCell(l_row, p_newColumn, l_width, l_height,
                                    l_cell);
        }

        // Sets a new Startpoint if the column was moved to position zero.
        if (p_guiColumn == 0) {
//            final int l_oldStart = this.m_positionXStart;
//            this.m_positionXStart = l_oldStart - l_width;

            // Position the first Column
            for (int l_row = 0;
                 l_row < this.m_poolCells.m_guiCells.amountRows();
                 l_row++) {
                this.repositionCell(
                    this.m_poolCells.m_guiCells.get(l_row, p_guiColumn));
            }
        }

        //Reposition all following columns.
        for (int l_column = p_guiColumn;
             l_column < this.m_poolCells.m_guiCells.amountColumns();
             l_column++) {
            for (int l_row = 0;
                 l_row < this.m_poolCells.m_guiCells.amountRows();
                 l_row++) {
                this.repositionCell(
                    this.m_poolCells.m_guiCells.get(l_row, l_column));
            }
        }
    }

    /**
     * Rewrite the visual rules for the textfield of p_guiRow with content
     * p_newRow (Text, width, height, background-colour, text-colour).
     *
     * @param p_guiRow Graphic row to reinitialize.
     * @param p_newRow Cell-contents to feed into the row.
     */
    protected void pUpdateRowContent(final int p_guiRow, final int p_newRow)
    {
        // Load new cell contents into the row
        final int l_height = this.m_poolData.m_heightRows.get(p_newRow);
        // Amount columns = Cells in row
        for (int l_i = 0;
             l_i < this.m_poolCells.m_guiCells.amountColumns();
             l_i++) {
            final @NotNull CCellLoaded l_guiCell =
                this.m_poolCells.m_guiCells.get(p_guiRow, l_i);
            //this.m_guiCells.get(l_i, p_guiRow);
            final int l_column = l_guiCell.getLoadedCellColumn();
            final ICell<String> l_cell =
                this.m_poolData.m_dataTable.getColumnRow(l_column, p_newRow);
            final double l_width =
                this.m_poolData.m_widthColumns.get(l_column).get();
            l_guiCell.setLoadedCell(p_newRow, l_column, l_width, l_height,
                                    l_cell);
        }

        // Sets a new Startpoint if the column was moved to position zero.
        if (p_guiRow == 0) {
//            final int l_oldStart = this.m_positionYStart;
//            this.m_positionYStart = l_oldStart - l_height;

            // Position the first Row
            for (int l_column = 0;
                 l_column < this.m_poolCells.m_guiCells.amountColumns();
                 l_column++) {
                this.repositionCell(
                    this.m_poolCells.m_guiCells.get(p_guiRow, l_column));
            }
        }

        // The row is not on the first position. Update all following rows
        for (int l_row = p_guiRow;
             l_row < this.m_poolCells.m_guiCells.amountRows();
             l_row++) {
            for (int l_column = 0;
                 l_column < this.m_poolCells.m_guiCells.amountColumns();
                 l_column++) {
                this.repositionCell(
                    this.m_poolCells.m_guiCells.get(l_row, l_column));
            }
        }
    }

    /**
     * Corrects the positioning of the current loaded cell.
     *
     * @param p_cellGUI GUI cell to reposition.
     */
    private void repositionCell(final @NotNull CCellLoaded p_cellGUI)
    {
        final int l_loadedColumn = p_cellGUI.getLoadedCellColumn();
        final int l_loadedRow = p_cellGUI.getLoadedCellRow();

        final double l_posX;
        if (l_loadedColumn == 0) {
            l_posX = 0 - this.m_poolData.m_positionXStart;
        } else {
            l_posX = this.m_poolData.m_positionColumns.get(l_loadedColumn).get()
                - this.m_poolData.m_positionXStart;
        }
        final int l_posY;
        if (l_loadedRow == 0) {
            l_posY = 0 - this.m_poolData.m_positionYStart;
        } else {
            l_posY = this.m_poolData.m_heightRowsSum.get(l_loadedRow - 1)
                - this.m_poolData.m_positionYStart;
        }
        AnchorPane.setLeftAnchor(p_cellGUI, l_posX);
        AnchorPane.setTopAnchor(p_cellGUI, (double) l_posY);

    }

    /**
     * Moves the table to an absolute position and repaints the whole table.
     * Not intended for small shifting. Please use shiftTable for that.
     * Not Error safe.
     *
     * @param p_pixelAbsolutHorizontal Absolute position of the left corner in
     *                                 the table in pixel.
     * @param p_pixelAbsolutVertical   Absolute position of the top corner in
     *                                 the table in pixel.
     */
    protected void pPositionTableAbs(
        final int p_pixelAbsolutHorizontal, final int p_pixelAbsolutVertical)
    {
        this.m_poolData.m_positionXStart = p_pixelAbsolutHorizontal;
        this.m_poolData.m_positionYStart = p_pixelAbsolutVertical;
        final int l_amountColumns = this.m_poolCells.m_guiCells.amountColumns();
        final int l_amountRows = this.m_poolCells.m_guiCells.amountRows();
        int l_firstCellIndexRow = 0;
        int l_firstCellIndexColumn = 0;
        for (int l_column = 0;
             l_column < this.m_poolData.m_positionColumns.size();
             l_column++) {
            if (this.m_poolData.m_positionColumns.get(l_column).get()
                >= this.m_poolData.m_positionXStart)
            {
                l_firstCellIndexColumn = l_column;
                break;
            }
        }
        for (int l_row = 0;
             l_row < this.m_poolData.m_heightRowsSum.size();
             l_row++) {
            if (this.m_poolData.m_heightRowsSum.get(l_row)
                >= this.m_poolData.m_positionYStart)
            {
                l_firstCellIndexRow = l_row;
                break;
            }
        }

        for (int l_column = 0; l_column < l_amountColumns; l_column++) {
            for (int l_row = 0; l_row < l_amountRows; l_row++) {
                final int l_loadedCellColumn =
                    l_column + l_firstCellIndexColumn;
                final int l_loadedCellRow = l_row + l_firstCellIndexRow;
                final double l_width =
                    this.m_poolData.m_widthColumns.get(l_loadedCellColumn)
                                                  .get();
                final int l_height =
                    this.m_poolData.m_heightRows.get(l_loadedCellRow);
                final ICell<String> l_cell =
                    this.m_poolData.m_dataTable.getColumnRow(l_loadedCellColumn,
                                                             l_loadedCellRow);
                this.m_poolCells.m_guiCells.get(l_row, l_column)
                                           .setLoadedCell(l_loadedCellRow,
                                                          l_loadedCellColumn,
                                                          l_width, l_height,
                                                          l_cell);
                this.repositionCell(
                    this.m_poolCells.m_guiCells.get(l_row, l_column));
            }
        }
    }

    /**
     * Text of Cell changed.
     *
     * @param p_cell Cell that has a changed content.
     * @param p_old  Old content.
     * @param p_new  New content.
     */
    private void doOnIListenerCellChanged(
        final @NotNull ICell<String> p_cell,
        final @Nullable String p_old,
        final @Nullable String p_new)
    {
        // TODO imnplement me
        //throw new NotImplementedException("Not yet implemented");

    }

    /**
     * Colour of cell changed.
     *
     * @param p_cell Cell that has a changed colour.
     * @param p_old  Old colour.
     * @param p_new  New colour.
     */
    private void doOnIListenerCellChangedColour(
        final @NotNull ICell<String> p_cell,
        final @NotNull Color p_old,
        final @NotNull Color p_new)
    {
        final CCellLoaded l_firstLoadedCell =
            this.m_poolCells.m_guiCells.get(0, 0);
        final TablePosition l_firstCell =
            new TablePosition(l_firstLoadedCell.getLoadedCellRow(),
                              l_firstLoadedCell.getLoadedCellColumn());
        final TablePosition l_lastCell = new TablePosition(
            l_firstCell.getRow() + this.m_poolCells.m_guiCells.amountColumns()
                - 1,
            l_firstCell.getColumn() + this.m_poolCells.m_guiCells.amountRows()
                - 1);
        final TablePosition l_changedCell =
            new TablePosition(p_cell.getIndexRow().getInt(),
                              p_cell.getIndexColumn().getInt());

        if (l_changedCell.isInBounds(l_firstCell, l_lastCell)) {
            final int l_diffRow = l_changedCell.getRow() - l_firstCell.getRow();
            final int l_diffCol =
                l_changedCell.getColumn() - l_firstCell.getColumn();
            final CCellLoaded l_cell =
                this.m_poolCells.m_guiCells.get(l_diffRow, l_diffCol);
            if (l_cell.getLoadedCellRow() != l_changedCell.getRow()
                || l_cell.getLoadedCellColumn() != l_changedCell.getColumn())
            {
                try {
                    throw new Exception("Hier passt was nicht mit den Indices");
                } catch (@NotNull final Exception l_e) {
                    Log.inst().error(l_e.getMessage(), "EXCEPTION");
                    l_e.printStackTrace();
                }
            } else {
                l_cell.setBackgoundColour(p_new);
            }
        }
    }

    /**
     * Row was removed or added from data.
     *
     * @param p_stringIRow Removed or added row.
     * @param p_i          Row index that was removed or added.
     * @param p_eStatus    Indicator if removed or added.
     */
    private void doOnListenerAddRemoveRow(
        @NotNull final IRow<String> p_stringIRow,
        final int p_i,
        @NotNull final EStatus p_eStatus)
    {
        // TODO imnplement me
        throw new NotImplementedException("Not yet implemented");

    }

    /**
     * Column was removed or added from data.
     *
     * @param p_stringIColumn Removed or added column.
     * @param p_i             Column index that was removed or added.
     * @param p_eStatus       Indicator if removed or added.
     */
    private void doOnListenerAddRemoveColumn(
        @NotNull final IColumn<String> p_stringIColumn,
        final int p_i,
        @NotNull final EStatus p_eStatus)
    {
        // TODO imnplement me
        throw new NotImplementedException("Not yet implemented");

    }


    /**
     * Returns the max width (Position where the last cell ends).
     *
     * @return maximal width.
     */
    public ObservableDoubleValue getWidthMaxProperty()
    {
        return this.m_poolData.m_widthMax;
    }

    /**
     * Returns the max height (Position where the last cell ends).
     *
     * @return maximal height.
     */
    public ObservableDoubleValue getHeightMaxProperty()
    {
        return this.m_poolData.m_heightMax;
    }

    /**
     * Returns the current width of the view.
     *
     * @return current width.
     */
    public ReadOnlyIntegerProperty getWidthCurrentProperty()
    {
        return this.m_guiTableWidth;
    }

    /**
     * Returns the current height if the view.
     *
     * @return current height.
     */
    public ReadOnlyIntegerProperty getHeightCurrentProperty()
    {
        return this.m_guiTableHeight;
    }

    /**
     * Top left position anchor in the table in pixel shown on the UI.
     *
     * @return X position of where the view starts.
     */
    public int getPositionXStart()
    {
        return this.m_poolData.m_positionXStart;
    }

    /**
     * Top left position anchor in the table in pixel shown on the UI.
     *
     * @return Y position of where the view starts.
     */
    public int getPositionYStart()
    {
        return this.m_poolData.m_positionYStart;
    }

    /**
     * Returns the intern content table.
     *
     * @return content that is displayed.
     */
    @NotNull
    public InformationList<String> getDataTable()
    {
        return this.m_poolData.m_dataTable;
    }

    /**
     * Sortable container of rendered cells.
     * first = Row (width); second = Column (height)
     *
     * @return Gui cell container
     */
    @NotNull
    public VirtualArrayRowColumn<CCellLoaded> getGuiCells()
    {
        return this.m_poolCells.m_guiCells;
    }

    @Override
    public final ObservableDoubleValue getColumnPosX(final int p_column)
    {
        return this.m_poolData.m_positionColumns.get(p_column);
    }

    @Override
    public final ObservableDoubleValue getColumnWidth(final int p_column)
    {
        return this.m_poolData.m_widthColumns.get(p_column);
    }
}
