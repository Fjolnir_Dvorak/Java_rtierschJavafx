/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import de.rtiersch.datatypes.notnull.shared.IIntegerRead;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import org.jetbrains.annotations.NotNull;

public abstract class TextfieldTwoStatesInput
    extends StackPane {
    protected final @NotNull TextField m_textField;
    protected IIntegerRead m_column;
    protected boolean m_replacenmentState;
    protected String m_pattern;

    public TextfieldTwoStatesInput(final IIntegerRead p_column) {
        this.m_column = p_column;
        this.m_textField = new TextField();
        super.getChildren().add(this.m_textField);
        this.m_textField.setPromptText("Neue Regel erstellen.");
        this.m_textField.setOnKeyReleased(this::handleKeyEvent);
        this.m_textField.prefHeightProperty()
            .bind(super.prefHeightProperty());
        this.m_textField.prefWidthProperty()
            .bind(super.prefWidthProperty());
        this.m_textField.minHeightProperty()
            .bind(super.minHeightProperty());
        this.m_textField.minWidthProperty()
            .bind(super.minWidthProperty());
        this.m_textField.maxHeightProperty()
            .bind(super.maxHeightProperty());
        this.m_textField.maxWidthProperty()
            .bind(super.maxWidthProperty());
    }

    private void handleKeyEvent(final @NotNull KeyEvent p_event) {
        if (p_event.getCode() == KeyCode.ENTER) {
            if (!this.m_replacenmentState) {
                this.m_pattern = this.m_textField.getText();
                this.m_textField.setText("");
                this.m_textField.setPromptText("Neuer Wert");
                this.m_textField.setStyle("-fx-background-color: #ffefef");
                this.m_replacenmentState = true;
            } else {
                this.handleInput(this.m_pattern, this.m_textField.getText());
                this.m_textField.setText("");
                this.m_textField.setPromptText("Neue Regel erstellen.");
                this.m_textField.setStyle("-fx-background-color: white;");
                this.m_pattern = "";
                this.m_replacenmentState = false;
                super.getParent().requestFocus();
            }
        }
    }

    protected abstract void handleInput(final String p_pattern,
        final String p_text);
}
