/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import de.rtiersch.datatypes.notnull.shared.IIntegerRead;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import org.jetbrains.annotations.NotNull;

public abstract class TextfieldOneStateInput
    extends StackPane {
    protected final @NotNull TextField m_textField;
    protected IIntegerRead m_column;

    public TextfieldOneStateInput(final IIntegerRead p_column) {
        this.m_column = p_column;
        this.m_textField = new TextField();
        super.getChildren().add(this.m_textField);
        this.m_textField.setPromptText("Neue Regel erstellen.");
        this.m_textField.setOnKeyReleased(this::handleKeyEvent);
    }

    private void handleKeyEvent(final @NotNull KeyEvent p_event) {
        if (p_event.getCode() == KeyCode.ENTER) {
            this.handleInput(this.m_textField.getText());
            this.m_textField.setText("");
            this.m_textField.setPromptText("Neue Regel erstellen.");
            this.m_textField.setStyle("-fx-background-color: white;");
            super.getParent().requestFocus();
        }
    }

    protected abstract void handleInput(final String p_text);
}
