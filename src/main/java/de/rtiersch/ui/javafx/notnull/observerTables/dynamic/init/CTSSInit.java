/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic.init;

import com.sun.javafx.tk.Toolkit;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components.CCellLoaded;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IColumn;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IColumns;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IRow;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.IRows;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CTSSInit
{
    /**
     * Standard width for Textfields.
     */
    public static final int GUI_CELL_DEFAULT_WIDTH = 149;
    /**
     * Default font used for viewing the data. Please do only use monospaced
     * fonts to ensure a fast parsing of the raw data to calculate the text
     * length.
     */
    @NotNull
    public static final Font GUI_FONT = Font.loadFont(
        Class.class.getResourceAsStream(
            "/assets/fonts/inconsolata/Inconsolata-Regular" + ".ttf"), 12);
    /**
     * Sum padding left and right of the textfield used to display the text
     * of the cells.
     */
    public static final int GUI_PADDING_TEXTFIELD_LR = 14;
    /**
     * Sum padding top and bottom of the textfield used to display the text
     * of the cells.
     */
    public static final int GUI_PADDING_TEXTFIELD_TB = 9;
    /**
     * Width of the Font used in the table.
     */
    public static final double GUI_FONT_WIDTH =
        Toolkit.getToolkit().getFontLoader().computeStringWidth("w", GUI_FONT);
    /**
     * Height of the Font used in the table.
     */
    public static final double GUI_FONT_HEIGHT = Toolkit.getToolkit()
                                                        .getFontLoader()
                                                        .getFontMetrics(
                                                            GUI_FONT)
                                                        .getLineHeight();
    /**
     * Standard height for textfields.
     */
    public static final int GUI_CELL_DEFAULT_HEIGHT =
        (int) GUI_FONT_HEIGHT + GUI_PADDING_TEXTFIELD_TB;
    /**
     * Performance and preload factor multiplied with DisplayMax to calculate
     * the amount of absolute cells which should be created.
     */
    public static final double PRELOAD_BALANCE = 1.5;

    /**
     * Static "utility" used for initialization. Keep class clean.
     */
    private CTSSInit()
    {

    }


    // TODO use sharedBoolenPropertyObservableArrayList
    public static ArrayList<DoubleProperty> InitCalculateColumnWidthProperty(
        final @NotNull InformationList<String> p_dataTable,
        final int p_cellMinWidth,
        final int p_cellMaxWidth)
    {
        final ArrayList<Integer> l_list =
            CTSSInit.InitCalculateColumnWidth(p_dataTable, p_cellMinWidth,p_cellMaxWidth);
        final ArrayList<DoubleProperty> l_return =
            new ArrayList<>(l_list.size());
        for (Integer l_integer : l_list) {
            l_return.add(new SimpleDoubleProperty(l_integer.doubleValue()));
        }
        return l_return;
    }

    public static ArrayList<Integer> InitCalculateColumnWidth(
        final @NotNull InformationList<String> p_dataTable,
        final int p_cellMinWidth,
        final int p_cellMaxWidth)
    {
        final IColumns<String> l_columns = p_dataTable.getColumns();
        final ArrayList<Integer> l_width = new ArrayList<>(l_columns.size());
        for (int l_i = 0; l_i < l_columns.size(); l_i++) {
            final IColumn<String> l_column = l_columns.getColumn(l_i);
            /*
             * find the longest string of column for mono.
             */
            int l_length = 0;
            for (int l_j = 0; l_j < l_column.size(); l_j++) {
                final String l_cell = l_column.getCell(l_j).getContent();
                if (l_cell.length() > l_length) {
                    l_length = l_cell.length();
                }
            }
            if (l_column.getHeader().length() > l_length) {
                l_length = l_column.getHeader().length();
            }
            /*
             * Adding padding to each longest length and saving it.
             */
            final int l_cell;
            l_cell = CTSSInit.CalculateCellWidth(l_length, p_cellMinWidth, p_cellMaxWidth);
            l_width.add(l_i, l_cell);
        }
        return l_width;
    }
    public static int InitCalculateHeaderColumnWidth(
        final @NotNull IRows<String> p_rows,
        final int p_cellMinWidth,
        final int p_cellMaxWidth)
    {
        int l_longestCell = 0;
        for (IRow<String> l_row : p_rows) {
            final int l_cellLength = l_row.getHeader().length();
            if (l_cellLength > l_longestCell) {
                l_longestCell = l_cellLength;
            }
        }
        return CTSSInit.CalculateCellWidth(l_longestCell, p_cellMinWidth, p_cellMaxWidth);
    }

    public static ArrayList<Integer> CalculateSum(
        final @NotNull ArrayList<Integer> p_singleSize)
    {
        final ArrayList<Integer> l_summedSize;
        l_summedSize = new ArrayList<>(p_singleSize.size());
        l_summedSize.add(p_singleSize.get(0));
        for (int l_i = 1; l_i < p_singleSize.size(); l_i++) {
            final int l_last = l_summedSize.get(l_i - 1);
            final int l_add = p_singleSize.get(l_i);
            l_summedSize.add(l_i, l_last + l_add);
        }
        return l_summedSize;
    }

    public static ArrayList<ObservableDoubleValue> CalculatePositions(
        final @NotNull ArrayList<DoubleProperty> p_singleSize)
    {
        final ArrayList<ObservableDoubleValue> l_summedSize;
        l_summedSize = new ArrayList<>(p_singleSize.size() + 1);
        l_summedSize.add(new SimpleDoubleProperty(0));
        for (int l_i = 1; l_i <= p_singleSize.size(); l_i++) {
            l_summedSize.add(l_i, p_singleSize.get(l_i - 1)
                                              .add(l_summedSize.get(l_i - 1)));
        }
        return l_summedSize;
    }

    /**
     * Guarantees that the cell has a minimal width and all the text fits
     * into it. Only works for Mono, but that#s no problem. I am using only
     * mono. It that will be changed, this will not work. Well, it will work,
     * but return false values.
     *
     * @param p_width        Current width of the cell.
     * @param p_cellMinWidth Minimal width of the cells.
     * @return new width of the cell
     */
    public static int CalculateCellWidth(
        final int p_width,
        final int p_cellMinWidth,
        final int p_cellMaxWidth)
    {
        final int l_width =
            (int) (p_width * GUI_FONT_WIDTH) + GUI_PADDING_TEXTFIELD_LR;
        if (l_width >= p_cellMinWidth) {
            return l_width;
        }
        return p_cellMinWidth;
    }

    public static ArrayList<Integer> InitCalculateColumnHeight(
        final @NotNull InformationList<String> p_dataTable)
    {
        final ArrayList<Integer> l_height;
        l_height = new ArrayList<>(p_dataTable.sizeRows());
        for (int l_i = 0; l_i < p_dataTable.sizeRows(); l_i++) {
            /*
             * Adding padding to each font height and saving it.
             */
            l_height.add(l_i, GUI_CELL_DEFAULT_HEIGHT);
        }
        return l_height;
    }

    public static int CalculatePreloadLR(
        final int p_maxWidth, final int p_defaultWidth, final int p_lengthRow)
    {
        final int l_minWidth;
        if (p_defaultWidth == 0) {
            l_minWidth = GUI_CELL_DEFAULT_WIDTH;
        } else {
            l_minWidth = p_defaultWidth;
        }
        final int l_toGenerate;
        l_toGenerate = (int) (p_maxWidth * PRELOAD_BALANCE / l_minWidth);

        if (p_lengthRow < l_toGenerate) {
            return p_lengthRow;
        }
        return l_toGenerate;
    }

    public static int CalculatePreloadTD(
        final int p_maxHeight, final int p_lengthColumn)
    {
        final int l_toGenerate;
        l_toGenerate =
            (int) (p_maxHeight * PRELOAD_BALANCE / GUI_CELL_DEFAULT_HEIGHT);

        if (p_lengthColumn < l_toGenerate) {
            return p_lengthColumn;
        }
        return l_toGenerate;
    }

    public static CCellLoaded[][] InitPreloadCells(
        final int p_toGenTD,
        final int p_toGenLR,
        final @NotNull InformationList<String> p_data,
        final @NotNull ArrayList<Integer> p_pixelHeight,
        final @NotNull ArrayList<Integer> p_pixelHeightSum,
        final @NotNull ArrayList<DoubleProperty> p_pixelWidth,
        final @NotNull ArrayList<ObservableDoubleValue> p_pixelColumnPosition)
    {
        final CCellLoaded[][] l_table = new CCellLoaded[p_toGenTD][p_toGenLR];
        for (int l_indexRow = 0; l_indexRow < l_table.length; l_indexRow++) {

            final CCellLoaded[] l_row = l_table[l_indexRow];
            final int l_posY;
            if (l_indexRow == 0) {
                l_posY = 0;
            } else {
                l_posY = p_pixelHeightSum.get(l_indexRow - 1);
            }
            final int l_height = p_pixelHeight.get(l_indexRow);

            for (int l_indexColumn = 0;
                 l_indexColumn < l_row.length;
                 l_indexColumn++) {

                /*
                 * Sets the position of this cell and anchor it.
                 */
                final double l_posX;
                l_posX = p_pixelColumnPosition.get(l_indexColumn).get();
                final double l_width = p_pixelWidth.get(l_indexColumn).get();
                final ICell<String> l_cellData =
                    p_data.getColumnRow(l_indexColumn, l_indexRow);
                final CCellLoaded l_cell =
                    new CCellLoaded(l_indexRow, l_indexColumn, l_width,
                                    l_height, l_cellData, CTSSInit.GUI_FONT);
                l_row[l_indexColumn] = l_cell;

                AnchorPane.setTopAnchor(l_cell, (double) l_posY);
                AnchorPane.setLeftAnchor(l_cell, l_posX);
            }

        }
        return l_table;
    }
}
