/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic;

import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.init.CTSStringHeader;


import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import org.jetbrains.annotations.NotNull;

/**
 * Scrollable graphical class to visualize InformationList, a rectangular
 * safe datatype specialized on efficiency and content safety.
 */
public class CTSString
    extends AnchorPane
    implements IColumnPositioner
{

    /**
     * Width of a scrollbar.
     */
    private static final double SCROLL_WIDTH = 14.0;
    /**
     * Cell Container.
     */
    @NotNull
    private final CTSStringContent m_ctssc;
    /**
     * Horizontal Scrollbar and scroll behaviour container.
     */
    @NotNull
    private final ScrollEnhanced m_scrollH;
    /**
     * Vertical Scrollbar and scroll behaviour container.
     */
    @NotNull
    private final ScrollEnhanced       m_scrollV;
    /**
     * Padding for m_ctssc dependend on wether the scrollpane is visual or not.
     */
    @NotNull
    private final SimpleDoubleProperty m_paddingRight;
    /**
     * Padding for m_ctssc dependend on wether the scrollpane is visual or not.
     */
    @NotNull
    private final SimpleDoubleProperty m_paddingBottom;
    /**
     * Header container containing also the Content.
     */
    @NotNull
    private final CTSStringHeader      m_ctssh;


    /**
     * Creates a new GUI Table based on JavaFX.
     *
     * @param p_maxWidth             Maximal width of this table.
     * @param p_maxHeight            Maximal height of this table.
     * @param p_cellMinWidth         Minimal width of the columns.
     * @param p_data                 Model to observe and to display.
     * @param p_showScrollHorizontal Visiility of scroll bar.
     * @param p_showScrollVertical   Visibility of scroll bar.
     * @param p_headerHVisible       Visiility of horizontal header.
     * @param p_headerVVisible       Visibility of vertical header.
     */
    public CTSString(
        final int p_maxWidth,
        final int p_maxHeight,
        final int p_cellMinWidth,
        @NotNull final InformationList<String> p_data,
        final boolean p_showScrollHorizontal,
        final boolean p_showScrollVertical,
        final boolean p_headerHVisible,
        final boolean p_headerVVisible)
    {
        this.m_ctssc =
            new CTSStringContent(p_maxWidth, p_maxHeight, p_cellMinWidth,
                                 p_data);
        this.m_ctssh = new CTSStringHeader(this.m_ctssc, p_headerHVisible,
                                           p_headerVVisible);
        this.m_scrollH =
            new ScrollEnhanced(this.m_ctssc.getWidthCurrentProperty(),
                               this.m_ctssc.getWidthMaxProperty(),
                               Orientation.HORIZONTAL);
        this.m_scrollV =
            new ScrollEnhanced(this.m_ctssc.getHeightCurrentProperty(),
                               this.m_ctssc.getHeightMaxProperty(),
                               Orientation.VERTICAL);

        /*
         * Add all Nodes position those on the Anchorpane.
         */
        super.getChildren().add(this.m_ctssh);
        super.getChildren().addAll(this.m_scrollH, this.m_scrollV);

        AnchorPane.setLeftAnchor(this.m_scrollH, 0.0);
        AnchorPane.setRightAnchor(this.m_scrollH, SCROLL_WIDTH);
        AnchorPane.setBottomAnchor(this.m_scrollH, 0.0);

        AnchorPane.setTopAnchor(this.m_scrollV, 0.0);
        AnchorPane.setBottomAnchor(this.m_scrollV, SCROLL_WIDTH);
        AnchorPane.setRightAnchor(this.m_scrollV, 0.0);

        AnchorPane.setTopAnchor(this.m_ctssh, 0.0);
        AnchorPane.setLeftAnchor(this.m_ctssh, 0.0);


        /*
         * Initialize Scrollbar visibility.
         */
        if (p_showScrollHorizontal) {
            AnchorPane.setBottomAnchor(this.m_ctssh, SCROLL_WIDTH);
            this.m_paddingBottom = new SimpleDoubleProperty(SCROLL_WIDTH);
            this.m_scrollH.setVisible(true);
        } else {
            AnchorPane.setBottomAnchor(this.m_ctssh, 0.0);
            this.m_paddingBottom = new SimpleDoubleProperty(0);
            this.m_scrollH.setVisible(false);
        }
        if (p_showScrollVertical) {
            AnchorPane.setRightAnchor(this.m_ctssh, SCROLL_WIDTH);
            this.m_paddingRight = new SimpleDoubleProperty(SCROLL_WIDTH);
            this.m_scrollV.setVisible(true);
        } else {
            AnchorPane.setRightAnchor(this.m_ctssh, 0.0);
            this.m_paddingRight = new SimpleDoubleProperty(0);
            this.m_scrollV.setVisible(false);
        }

        /*
         * Listen on visibility changes and handle those.
         */
        this.m_scrollH.visibleProperty()
                      .addListener(this::doOnHorizontalVisible);
        this.m_scrollV.visibleProperty().addListener(this::doOnVerticalVisible);

        /*
         * Bind the size of the scrollbars if they are visible to the next
         * deeper node CTSSHeader.
         */
        this.m_ctssh.prefWidthProperty()
                    .bind(super.widthProperty().subtract(this.m_paddingRight));
        this.m_ctssh.prefHeightProperty()
                    .bind(
                        super.heightProperty().subtract(this.m_paddingBottom));

        /*
         * Handle and sort scroll events. Events are captured and redirected
         * to the scrollbars to let them handle the scrolling. The scrollbar
         * itself will handle overscrolling for me so I do not have to bother
          * thinking about that.
         */
        this.m_scrollH.valueProperty().addListener(this::doOnScrollH);
        this.m_scrollV.valueProperty().addListener(this::doOnScrollV);
        super.addEventFilter(ScrollEvent.ANY, this::doOnScrollEvent);
    }

    /**
     * Catches all scroll events and redirects them to their apropiate
     * scrollbar.
     *
     * @param p_event Scroll event to consume or to sort or to let it pass
     *                through.
     */
    private void doOnScrollEvent(final ScrollEvent p_event)
    {

        if (!p_event.getTarget().equals(this.m_scrollH)
            && !p_event.getTarget().equals(this.m_scrollV))
        {
            p_event.consume();

            // Sort scroll events
            if (p_event.getDeltaX() != 0) {
                this.m_scrollH.fireEvent(p_event);
            }

            if (p_event.getDeltaY() != 0) {
                this.m_scrollV.fireEvent(p_event);
            }
        }
    }

    /**
     * Scroll behaviour for horizontal scrolling.
     *
     * @param p_observable Not used.
     * @param p_oldValue   Old position the scrolling started at.
     * @param p_newValue   New position after scrolling.
     */
    private void doOnScrollH(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue)
    {
        final int l_realStart = this.m_ctssc.getPositionXStart();
        final int l_shift = p_newValue.intValue() - l_realStart;
        this.m_ctssc.shiftTable(l_shift, 0);
    }

    /**
     * Scroll behaviour for vertical scrolling.
     *
     * @param p_observable Not used.
     * @param p_oldValue   Old position the scrolling started at.
     * @param p_newValue   New position after scrolling.
     */
    private void doOnScrollV(
        final ObservableValue<? extends Number> p_observable,
        final Number p_oldValue,
        final Number p_newValue)
    {
        final int l_realStart = this.m_ctssc.getPositionYStart();
        final int l_shift = p_newValue.intValue() - l_realStart;
        this.m_ctssc.shiftTable(0, l_shift);
    }

    /**
     * Is the scroll pane visible? If not change the bottom padding to zero,
     * if yes change it to 14.
     *
     * @param p_observable not used.
     * @param p_oldValue   old visibility.
     * @param p_newValue   new visibility.
     */
    private void doOnHorizontalVisible(
        final ObservableValue<? extends Boolean> p_observable,
        final Boolean p_oldValue,
        final Boolean p_newValue)
    {
        if (p_newValue.equals(p_oldValue)) {
            // Do not use == for comparison. Compare content, not pointer.
            return; // Dirty Event. Nothing happened
        }
        if (p_newValue) {
            AnchorPane.setRightAnchor(this.m_ctssh, SCROLL_WIDTH);
            this.m_paddingRight.set(SCROLL_WIDTH);
            return;
        }
        AnchorPane.setRightAnchor(this.m_ctssh, 0.0);
        this.m_paddingRight.set(0);
    }

    /**
     * Is the scroll pane visible? If not change the right padding to zero,
     * if yes change it to 14.
     *
     * @param p_observable not used.
     * @param p_oldValue   old visibility.
     * @param p_newValue   new visibility.
     */
    private void doOnVerticalVisible(
        final ObservableValue<? extends Boolean> p_observable,
        final Boolean p_oldValue,
        final Boolean p_newValue)
    {
        if (p_newValue.equals(p_oldValue)) {
            // Do not use == for comparison. Compare content, not pointer.
            return; // Dirty Event. Nothing happened
        }
        if (p_newValue) {
            AnchorPane.setBottomAnchor(this.m_ctssh, SCROLL_WIDTH);
            this.m_paddingBottom.set(SCROLL_WIDTH);
            return;
        }
        AnchorPane.setBottomAnchor(this.m_ctssh, 0.0);
        this.m_paddingBottom.set(0);
    }

    /**
     * Returns the scrollbar for horizontal scrolling.
     *
     * @return active scrollbar.
     */
    public ScrollBar getScrollBarH()
    {
        return this.m_scrollH;
    }

    /**
     * Returns the scrollbar for vertical scrolling.
     *
     * @return active scrollbar.
     */
    public ScrollBar getScrollBarV()
    {
        return this.m_scrollV;
    }

    @Override
    public final ObservableDoubleValue getColumnPosX(final int p_column)
    {
        return this.m_ctssc.getColumnPosX(p_column);
    }

    @Override
    public final ObservableDoubleValue getColumnWidth(final int p_column)
    {
        return this.m_ctssc.getColumnWidth(p_column);
    }

    /**
     * Returns the width of the header column (the left header going down).
     *
     * @return Width of Header.
     */
    public double getHeaderColumnWidth()
    {
        return this.m_ctssh.getHeaderWidth();
    }
}
