/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components;

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellGui;
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellHasGui;
import javafx.scene.text.Font;
import org.jetbrains.annotations.NotNull;

public class CCellLoaded
    extends CCellOptimized
    implements ICellGui
{

    private int m_loadedCellRow;
    private int m_loadedCellColumn;
    private ICell<String> m_cell;

    public CCellLoaded(
        final int p_loadedCellRow,
        final int p_loadedCellColumn,
        final double p_width,
        final int p_height,
        final ICell<String> p_cell,
        final @NotNull Font p_guiFont) {
        super.setText(p_cell.getContent());
        super.setBackgoundColour(p_cell.getBackgroundColour());
        super.setPrefSize(p_width, p_height);
        super.setMaxSize(p_width, p_height);
        this.m_loadedCellColumn = p_loadedCellColumn;
        this.m_loadedCellRow = p_loadedCellRow;
        this.m_cell = p_cell;
        if (this.m_cell instanceof ICellHasGui) {
            ((ICellHasGui) this.m_cell).setGuiCell(this);
        }
    }

    public void setLoadedCell(final int p_loadedCellRow,
                              final int p_loadedCellColumn,
                              final double p_width,
                              final int p_height,
                              @NotNull final ICell<String> p_cell) {
        super.setText(p_cell.getContent());
        super.setBackgoundColour(p_cell.getBackgroundColour());
        super.setPrefSize(p_width, p_height);
        this.m_loadedCellColumn = p_loadedCellColumn;
        this.m_loadedCellRow = p_loadedCellRow;
        if (this.m_cell instanceof ICellHasGui) {
            ((ICellHasGui) this.m_cell).unsetGuiCell(this);
        }
        this.m_cell = p_cell;
        if (this.m_cell instanceof ICellHasGui) {
            ((ICellHasGui) this.m_cell).setGuiCell(this);
        }
    }

    public int getLoadedCellRow() {
        return this.m_loadedCellRow;
    }

    public int getLoadedCellColumn() {
        return this.m_loadedCellColumn;
    }

    @Override
    public void guiTextUpdateEvent() {
        this.setText(this.m_cell.getContent());
    }

    @Override
    public void guiBackgroundColourUpdateEvent() {
        super.setBackgoundColour(this.m_cell.getBackgroundColour());
    }
}
