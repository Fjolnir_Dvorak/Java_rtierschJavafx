/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables;

import de.rtiersch.datatypes.notnull.observables.ObservableValue2D;
import de.rtiersch.datatypes.notnull.observables.lists.IListFixedSize;
import de.rtiersch.datatypes.notnull.observables.lists.ObservableArrayList2D;
import de.rtiersch.datatypes.notnull.observables.lists
    .ObservableArrayList2DColumn;
import de.rtiersch.logging.Log;
import de.rtiersch.ui.javafx.notnull.ScrollPaneAccessibleHBar;
import de.rtiersch.ui.javafx.notnull.observerTables.dynamic.IColumnPositioner;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Raphael Tiersch on 06.09.2016.
 */
public class ColumnTableNode
    extends ScrollPaneAccessibleHBar
{

    protected final @NotNull HBox m_table;
    private final ReadOnlyDoubleProperty m_cellHeight;
    private final IColumnPositioner m_columnPositioner;

    public ColumnTableNode(
        final @NotNull ObservableArrayList2DColumn<Node> p_table,
        final @NotNull IColumnPositioner p_columnPositioner,
        final @NotNull ReadOnlyDoubleProperty
        p_cellHeight) {
        this.m_columnPositioner = p_columnPositioner;
        this.m_cellHeight = p_cellHeight;
        final @NotNull
        IListFixedSize<IListFixedSize<ObservableValue2D<Node>>>
            l_observe = p_table.getColumns();
        this.m_table = new HBox();

        final int l_countColumns = l_observe.size();
        for (int l_i = 0; l_i < l_countColumns; l_i++) {
            final VBox l_vBox = new VBox();
            l_vBox.maxWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
            l_vBox.minWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
            l_vBox.prefWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
//            l_vBox.maxHeightProperty().bind(this.m_cellHeight);
//            l_vBox.minHeightProperty().bind(this.m_cellHeight);
//            l_vBox.prefHeightProperty().bind(this.m_cellHeight);
            l_vBox.setFillWidth(true);
            this.m_table.getChildren().add(l_vBox);
            final int l_countElems = l_observe.get(l_i).size();
            for (int l_j = 0; l_j < l_countElems; l_j++) {
                l_vBox.getChildren().add(l_observe.get(l_i).get(l_j).get());
            }
        }

        p_table.addChangeListener(this::doOnChange);
        p_table.addColumnAddedListener(this::doOnColumnAdded);
        p_table.addColumnRemovedListener(this::doOnColumnRemoved);
        p_table.addDoubleIndexedAddedListener(this::doOnElemAdded);
        p_table.addDoubleIndexedRemovedListener(this::doOnElemRemoved);
        super.setContent(this.m_table);
    }

    private void initCell(final @NotNull Node p_cell) {
        p_cell.setStyle("-fx-border-color: #c0c0c0; -fx-border-width: 1;");
        this.extInitCell(p_cell);
    }

    // TODO add constructor supporting IColumnPositioner

    public ColumnTableNode(
        final @NotNull ObservableArrayList2D<Node> p_table,
        final @NotNull IColumnPositioner p_columnPositioner,
        final @NotNull ReadOnlyDoubleProperty p_cellHeight) {
        final IListFixedSize<IListFixedSize<ObservableValue2D<Node>>>
            l_observe = p_table.getColumns();
        this.m_table = new HBox();
        this.m_columnPositioner = p_columnPositioner;
        this.m_cellHeight = p_cellHeight;
        Log.inst().debug("Creating node Table. Columns to process: " +
         l_observe.size(), "GUI.Init");
        final int l_countColumns = l_observe.size();
        final int l_countElems = p_table.getRows().size();
        for (int l_i = 0; l_i < l_countColumns; l_i++) {
            final VBox l_vBox = new VBox();
            l_vBox.maxWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
            l_vBox.minWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
            l_vBox.prefWidthProperty()
                .bind(this.m_columnPositioner.getColumnWidth(l_i));
//            l_vBox.maxHeightProperty().bind(this.m_cellHeight);
//            l_vBox.minHeightProperty().bind(this.m_cellHeight);
//            l_vBox.prefHeightProperty().bind(this.m_cellHeight);
            l_vBox.setFillWidth(true);
            this.m_table.getChildren().add(l_vBox);
            for (int l_j = 0; l_j < l_countElems; l_j++) {
                l_vBox.getChildren().add(l_observe.get(l_i).get(l_j).get());
            }
        }
        Log.inst().debug("Created Columns: " +
            this.m_table.getChildren().size(), "GUI.Init");

        p_table.addChangeListener(this::doOnChange);
        p_table.addRowAddedListener(this::doOnRowAdded);
        p_table.addRowRemovedListener(this::doOnRowRemoved);
        p_table.addColumnAddedListener(this::doOnColumnAdded);
        p_table.addColumnRemovedListener(this::doOnColumnRemoved);
        super.setContent(this.m_table);
    }

    private void doOnRowRemoved(
        final IListFixedSize<ObservableValue2D<Node>> p_value,
        final int p_i) {
        for (final Node l_node : this.m_table.getChildren()) {
            final VBox l_column = (VBox) l_node;
            l_column.getChildren().remove(p_i);
        }
        this.extDoOnRowRemoved(p_value);
    }

    private void doOnRowAdded(final @NotNull IListFixedSize<ObservableValue2D<Node>> p_value,
                              final int p_i) {
        final int l_countColumn = this.m_table.getChildren().size();
        final int l_y = p_value.get(0).getIndexY().getInt();
        for (int l_i = 0; l_i < l_countColumn; l_i++) {
            final VBox l_column = (VBox) this.m_table.getChildren().get(l_i);
            this.initCell(p_value.get(l_i).get());
            l_column.getChildren().add(l_y, p_value.get(l_i).get());
        }
        this.extDoOnRowAdded(p_value);
    }

    private void doOnElemRemoved(
        final @NotNull ObservableValue2D<Node> p_value) {
        final int l_x = p_value.getIndexX().getInt();
        final int l_y = p_value.getIndexY().getInt();
        ((VBox) this.m_table.getChildren().get(l_x)).getChildren().remove(l_y);
        this.extDoOnElemAdded(p_value);
    }

    private void doOnElemAdded(
        final @NotNull ObservableValue2D<Node> p_value) {
        final int l_x = p_value.getIndexX().getInt();
        final int l_y = p_value.getIndexY().getInt();
        final Node l_s = p_value.get();

        this.initCell(l_s);

        ((VBox) this.m_table.getChildren().get(l_x)).getChildren()
            .add(l_y, l_s);
        this.extDoOnElemAdded(p_value);
    }

    private void doOnColumnRemoved(
        final IListFixedSize<ObservableValue2D<Node>> p_value,
        final int p_i) {
        this.m_table.getChildren().remove(p_i);
        this.extDoOnColumnRemoved(p_value);
    }

    private void doOnColumnAdded(final @NotNull IListFixedSize<ObservableValue2D<Node>> p_value,
                                 final int p_i) {
        final int l_countElems = p_value.size();
        final VBox l_vBox = new VBox();
        l_vBox.maxWidthProperty()
            .bind(this.m_columnPositioner.getColumnWidth(p_i));
        l_vBox.minWidthProperty()
            .bind(this.m_columnPositioner.getColumnWidth(p_i));
        l_vBox.prefWidthProperty()
            .bind(this.m_columnPositioner.getColumnWidth(p_i));
//        l_vBox.maxHeightProperty().bind(this.m_cellHeight);
//        l_vBox.minHeightProperty().bind(this.m_cellHeight);
//        l_vBox.prefHeightProperty().bind(this.m_cellHeight);
        l_vBox.setFillWidth(true);
        for (int l_i = 0; l_i < l_countElems; l_i++) {
            this.initCell(p_value.get(l_i).get());
            l_vBox.getChildren().add(p_value.get(l_i).get());
        }
        this.m_table.getChildren().add(p_i, l_vBox);
        this.extDoOnColumnAdded(p_value);
    }

    private void doOnChange(final int p_x, final int p_y, final Node p_old,
        final Node p_new) {
        ((VBox) this.m_table.getChildren().get(p_x)).getChildren()
            .set(p_y, p_new);
        this.extDoOnChange(p_x, p_y, p_old, p_new);
    }



    protected void extDoOnColumnAdded(
        final @NotNull IListFixedSize<ObservableValue2D<Node>>
            p_value) {
    }

    protected void extDoOnChange(final int p_x, final int p_y,
        final Node p_old, final Node p_new) {
    }

    protected void extDoOnColumnRemoved(
        final IListFixedSize<ObservableValue2D<Node>> p_value) {

    }

    protected void extDoOnElemAdded(
        final @NotNull ObservableValue2D<Node> p_value) {
    }

    protected void extDoOnRowAdded(
        final @NotNull IListFixedSize<ObservableValue2D<Node>>
            p_value) {
    }

    protected void extDoOnRowRemoved(
        final IListFixedSize<ObservableValue2D<Node>> p_value) {
    }

    protected void extCellChanged(final Node p_oldValue,
        final Node p_newValue, final int p_iF, final int p_jF) {
    }

    protected void extInitCell(final @NotNull Node p_cell) {
    }

    public ObservableList<Node> getTable() {
        return this.m_table.getChildren();
    }
}
