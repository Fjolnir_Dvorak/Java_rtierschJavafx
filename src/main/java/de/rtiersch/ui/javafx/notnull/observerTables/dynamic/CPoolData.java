/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic;/*
 * Created by tiers on 15.06.2017.
 */

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CPoolData
{
    /**
     * Data table to be viewn on the UI.
     */
    @NotNull
    public final InformationList<String> m_dataTable;

    /**
     * Width of each column in pixel.
     */
    @NotNull
    public final ArrayList<DoubleProperty> m_widthColumns;
    /**
     * Height of each row in pixel.
     */
    @NotNull
    public final ArrayList<Integer> m_heightRows;
    /**
     * Absolute end pixel of each column.
     */
    @NotNull
    public final ArrayList<ObservableDoubleValue> m_positionColumns;
    /**
     * Absolute end pixel of each row.
     */
    @NotNull
    public final ArrayList<Integer> m_heightRowsSum;

    /**
     * Same as m_heightRowsSum({last element}}.
     * Used for easier readability.
     */
    public final SimpleDoubleProperty m_heightMax;
    /**
     * The summed up width of all columns. Used for boundary check.
     */
    public final SimpleDoubleProperty m_widthMax;
    /**
     * Top left position anchor in the table in pixel shown on the UI.
     */
    public int m_positionXStart;
    /**
     * Top left position anchor in the table in pixel shown on the UI.
     */
    public int m_positionYStart;

    public CPoolData(
        final InformationList<String> p_dataTable,
        final @NotNull ArrayList<DoubleProperty> p_widthColumns,
        final @NotNull ArrayList<Integer> p_heightRows,
        final @NotNull ArrayList<ObservableDoubleValue> p_positionColumns,
        final @NotNull ArrayList<Integer> p_heightRowsSum,
        final SimpleDoubleProperty p_heightMax,
        final SimpleDoubleProperty p_widthMax) {
        this.m_dataTable = p_dataTable;

        this.m_widthColumns = p_widthColumns;
        this.m_heightRows = p_heightRows;
        this.m_positionColumns = p_positionColumns;
        this.m_heightRowsSum = p_heightRowsSum;
        this.m_heightMax = p_heightMax;
        this.m_widthMax = p_widthMax;
    }
}
