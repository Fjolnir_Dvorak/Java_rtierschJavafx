/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.geometry.Orientation;
import javafx.scene.control.ScrollBar;
import org.jetbrains.annotations.NotNull;

public class ScrollEnhanced extends ScrollBar {
    private final ObservableDoubleValue m_maxProperty;
    private final ReadOnlyIntegerProperty m_currentProperty;

    public ScrollEnhanced(
        @NotNull final ReadOnlyIntegerProperty p_currentProperty,
        @NotNull final ObservableDoubleValue p_maxProperty,
        @NotNull final Orientation p_orientation) {
        this.m_maxProperty = p_maxProperty;
        this.m_currentProperty = p_currentProperty;

        super.setUnitIncrement(10.0);
        super.maxProperty().bind(DoubleBinding.doubleExpression(this
            .m_maxProperty).subtract(this.m_currentProperty));
        super.setOrientation(p_orientation);
    }
}
