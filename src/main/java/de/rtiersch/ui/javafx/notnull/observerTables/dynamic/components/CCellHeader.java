/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.jetbrains.annotations.NotNull;

public class CCellHeader
    extends CCellOptimized
{

    private int m_loadedCellIndex;

    public CCellHeader(
        final int p_loadedCellIndex,
        final int p_width,
        final int p_height,
        @NotNull final String p_text,
        @NotNull final Color p_backgroundColour,
        @NotNull final Font p_font)
    {
        super.setFont(p_font);
        this.setLoadedCell(p_loadedCellIndex, p_width, p_height, p_text,
                           p_backgroundColour);
        super.setEditable(false);
    }

    public void setLoadedCell(
        final int p_loadedCellIndex,
        final int p_width,
        final int p_height,
        @NotNull final String p_text,
        @NotNull final Color p_backgroundColour)
    {
        super.setText(p_text);
        super.setBackgoundColour(p_backgroundColour);
        super.setPrefSize(p_width, p_height);
        this.m_loadedCellIndex = p_loadedCellIndex;
    }

    public int getLoadedCellIndex()
    {
        return this.m_loadedCellIndex;
    }
}
