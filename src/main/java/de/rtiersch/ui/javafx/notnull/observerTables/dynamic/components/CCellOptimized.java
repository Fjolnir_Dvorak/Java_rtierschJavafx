/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull.observerTables.dynamic.components;

import de.rtiersch.ui.javafx.notnull.events.MouseEventFactory;
import de.rtiersch.util.Colour;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

public class CCellOptimized extends TextField
{
    /**
     * Changes the background colour for this TextField.
     * @param p_colour New colour.
     */
    public void setBackgoundColour(final @NotNull Color p_colour) {
        super.setStyle(
            "-fx-control-inner-background: " + Colour.getCSS(p_colour));
        super.addEventFilter(MouseEvent.ANY, this::mouseEventFilter);
    }

    private <T extends Event> void mouseEventFilter(final MouseEvent p_event) {
        // Some Elements listening von Click and Press (Like Textfield). I
        // want to capture both of these.
        if (!p_event.getEventType().equals(MouseEvent.MOUSE_CLICKED)
            && !p_event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
            return;
        }
        final Node l_target = (Node) p_event.getTarget();
        final Node l_focus = l_target.getScene().getFocusOwner();
        if (l_target.equals(l_focus)) {
            // Let the Node handle the event. It has already focus.
            return;
        }
        if (l_target == this) {
            return;
        }
        p_event.consume();
        MouseEvent l_mouseEvent = MouseEventFactory.copy(p_event).generate();
        MouseEvent.fireEvent(this, l_mouseEvent);
    }

    public void addToPosition(final int p_pixelHorizontal,
                              final int p_pixelVertical) {
        final double l_currentPosTop = AnchorPane.getTopAnchor(this);
        final double l_currentPosLeft = AnchorPane.getLeftAnchor(this);
        AnchorPane.setTopAnchor(this, l_currentPosTop + p_pixelVertical);
        AnchorPane.setLeftAnchor(this, l_currentPosLeft + p_pixelHorizontal);
    }
}
