/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import javafx.scene.control.TextField;

/**
 * This textfield allows you to filter the input by regex (and|or) restrict the
 * input to a fix length. Attention: The regex need to be valid on each typed
 * character. If you want to have six numbers, you have to allow all amount of
 * numbers below: [0-9]{0,6}.
 */
public class TextFieldRegex
        extends TextField {

    /**
     * The regular expression to verify the input.
     */
    private final String m_restictTo;
    /**
     * The maximal amount of allowed characters. 0 equals no restriction.
     */
    private final int m_maxChars;

    /**
     * Will validate the input on the regular expression.
     *
     * @param p_regex regular expression.
     */
    public TextFieldRegex(final String p_regex) {
        this.m_restictTo = p_regex;
        this.m_maxChars = 0;
    }

    /**
     * Will validate the input on the regular expression and will capture the
     * length of the textfield.
     *
     * @param p_regex  regular expression.
     * @param p_length length of textfield.
     */
    public TextFieldRegex(final String p_regex, final int p_length) {
        this.m_restictTo = p_regex;
        this.m_maxChars = p_length;
    }

    /**
     * Will capture the length of the textfield.
     *
     * @param p_length length of textfield.
     */
    public TextFieldRegex(final int p_length) {
        this.m_restictTo = ".*";
        this.m_maxChars = p_length;
    }

    @Override
    public final void replaceText(final int p_start, final int p_end,
            final String p_text) {
        if (this.matchTest(p_text)) {
            super.replaceText(p_start, p_end, p_text);
        }
    }

    @Override
    public final void replaceSelection(final String p_text) {
        if (this.matchTestReplace(p_text)) {
            super.replaceSelection(p_text);
        }
    }

    /**
     * Validates the input on a replace action. Called by this
     * .replaceSelection().
     *
     * @param p_text input.
     * @return true if matches, false if invalid.
     */
    private boolean matchTestReplace(final String p_text) {
        if (this.m_maxChars == 0) {
            return p_text.isEmpty() || ((super.getText()
                    .replaceAll(super.getSelectedText(), p_text)).matches(
                    this.m_restictTo));
        } else {
            return p_text.isEmpty() || ((super.getText()
                .replaceAll(super.getSelectedText(), p_text)).matches(
                this.m_restictTo) &&
                ((super.getText().length() + p_text.length()) <=
                    this.m_maxChars));
        }
    }

    /**
     * Vaidates the input on a regular type event. Called by
     * this.replaceText().
     *
     * @param p_text input.
     * @return true if matches, false if invalid.
     */
    private boolean matchTest(final String p_text) {
        if (this.m_maxChars == 0) {
            return p_text.isEmpty() ||
                    ((super.getText() + p_text).matches(this.m_restictTo));
        } else {
            return p_text.isEmpty() ||
                ((super.getText() + p_text).matches(this.m_restictTo) &&
                    ((super.getText().length() + p_text.length()) <=
                        this.m_maxChars));
        }
    }
}
