/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.VPos;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Raphael Tiersch on 20.09.2016.
 */
public class TextInPane extends AnchorPane implements ChangeListener<Bounds> {
    private final Text m_text;
    private final Rectangle m_rectangle = new Rectangle();

    public TextInPane() {
        this.m_text = new Text();
        this.getChildren().add(this.m_text);
    }
    public TextInPane(final @NotNull String p_text) {
        this.m_text = new Text(p_text);
        this.getChildren().add(this.m_text);
        super.setClip(this.m_rectangle);
        super.layoutBoundsProperty().addListener(this);
        this.m_text.setTextAlignment(TextAlignment.LEFT);
        this.m_text.setTextOrigin(VPos.CENTER);
        this.m_text.setFont(Font.getDefault());
        AnchorPane.setTopAnchor(this.m_text, 0.0);
        AnchorPane.setLeftAnchor(this.m_text, 0.0);
    }

    public StringProperty textProperty() {
        return this.m_text.textProperty();
    }

    public String getText() {
        return this.m_text.getText();
    }
    @Override
    public void changed(final ObservableValue<? extends Bounds> observable,
                        final Bounds oldValue,
                        final Bounds newValue) {
        this.m_rectangle.setWidth(newValue.getWidth());
        this.m_rectangle.setHeight(newValue.getHeight());
    }

    public void setText(final String p_new) {
        this.m_text.setText(p_new);
    }
}
