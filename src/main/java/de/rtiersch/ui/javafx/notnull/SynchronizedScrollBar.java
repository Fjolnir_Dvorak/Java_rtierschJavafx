/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.ui.javafx.notnull;

import javafx.scene.control.ScrollBar;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This custom ScrollBar is used to map the increment & decrement features to pixel based scrolling rather than
 * thumb/track based scrolling, if the "virtual" attribute is true.
 */
public class SynchronizedScrollBar
        extends ScrollBar
    implements IRegisterScrollBar {

    /**
     * Registered scrollbars which should be synchronized.
     */
    private final ArrayList<ScrollBar> m_scrollBars = new ArrayList<>();
    private boolean m_isInit;

    /**
     * Empty default constructor.
     */
    public SynchronizedScrollBar() {
        super.valueProperty().addListener(e -> this.updateValue());
    }

    /**
     * Default constructor with initialisation.
     *
     * @param p_scrollBars Adds scrollbars, which should be managed by this synchronized one.
     */
    public SynchronizedScrollBar(final @NotNull ScrollBar... p_scrollBars) {
        this.m_scrollBars.addAll(Arrays.asList(p_scrollBars));
        if (p_scrollBars.length > 0) {
            this.initializeScrollBar(p_scrollBars[0]);
        }
    }

    /**
     * Adds another scrollbar to the managed list.
     *
     * @param p_scrollBar Scrollbar, which should be managed by this class.
     */
    @Override
    public final void addScrollBar(final @NotNull ScrollBar... p_scrollBars) {
        this.m_scrollBars.addAll(Arrays.asList(p_scrollBars));
        if (!this.m_isInit) {
            if (p_scrollBars.length > 0) {
                this.initializeScrollBar(p_scrollBars[0]);
            }
        }
    }

    // this method is called when the user clicks in the scrollbar track, so
    // we special-case it to allow for page-up and page-down clicking to work
    // as expected.
//    @Override
//    public final void adjustValue(final double p_position) {
//        for (final ScrollBar l_scrollBar : this.m_scrollBars) {
//            l_scrollBar.adjustValue(p_position);
//        }
//
//        super.setValue(this.m_scrollBars.get(0).getValue());
//    }
//
//    @Override
//    public final void increment() {
//        for (final ScrollBar l_scrollBar : this.m_scrollBars) {
//            l_scrollBar.increment();
//        }
//
//        super.setValue(this.m_scrollBars.get(0).getValue());
//    }
//
//    @Override
//    public final void decrement() {
//        for (final ScrollBar l_scrollBar : this.m_scrollBars) {
//            l_scrollBar.decrement();
//        }
//
//        super.setValue(this.m_scrollBars.get(0).getValue());
//    }

    /**
     * Passes the current value to the managed scrollbars.
     */
    public final void updateValue() {
        final double l_relative = super.getValue() / super.getMax();

        for (final ScrollBar l_scrollBar : this.m_scrollBars) {
            l_scrollBar.setValue(l_scrollBar.getMax() * l_relative);
        }
    }

    public void initializeScrollBar(final ScrollBar p_bar) {
        if (this.m_isInit) {
            return;
        }
        this.m_isInit = true;
        super.setMin(p_bar.getMin());
        super.setMax(p_bar.getMax());
        super.setValue(p_bar.getValue());
        super.setUnitIncrement(p_bar.getUnitIncrement());
        super.setVisibleAmount(p_bar.getVisibleAmount());
    }
}
